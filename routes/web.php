<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' => ['incomplete-resume']], function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard.index');
    //Route::get('/home', 'HomeController@create')->name('home');
    Route::get('users/singleUser', 'admin\usersListController@singleUser')->name('users.singleUser');
    Route::get('/users/{id}/cvprofile', 'admin\usersListController@cvprofile')->name('users.cvprofile');
    Route::get('/users/{id}/edit', 'admin\usersListController@edit')->name('users.edit');
    Route::post('users/{id}/update', 'admin\usersListController@update')->name('users.update');
    Route::get('/cv/{id}/pdf', 'admin\usersListController@cv_pdf')->name('cv.cv_pdf');
    Route::get('delete-resume', 'admin\usersListController@destroy')->name('users.destroy');

});

Route::get('user/complete-resume','HomeController@completeResume');
Route::post('user/complete-resume','HomeController@completeResumeProcess');
Route::post('check-old-password','HomeController@checkOldPassword');


Route::get('/', function () {
    return view('index');
});

Route::get('/form', function () {
    return view('cvForm');
});

// Route::get('/email', function () {
//     return view('email.email');
// });

Route::get('webiste', 'FrontController@gotowebiste')->name('webiste.gotowebiste');
// Route::get('index_form/create', 'FrontController@index')->name('form.index');
Route::get('contact/create', 'FrontController@create_contact')->name('contact.create_contact');
Route::get('form/create', 'FrontController@create')->name('form.create');

Route::POST('form/store', 'FrontController@store')->name('form.store');

Route::get('download/{id}/pdfAction', 'FrontController@pdfAction')->name('download.pdfAction');
Route::get('download/{id}', 'FrontController@PdfDwonload_1')->name('download.PdfDwonload_1');
Route::get('download/{id}/PdfDwonload_2', 'FrontController@PdfDwonload_2')->name('download.PdfDwonload_2');
Route::get('download/{id}/PdfDwonload_3', 'FrontController@PdfDwonload_3')->name('download.PdfDwonload_3');
Route::get('download/{id}/viewPdf', 'FrontController@viewPdf')->name('download.viewPdf');
Route::get('download/canclePdf', 'FrontController@canclePdf')->name('download.canclePdf');


//Basic info
Route::post('add-basic-info', 'FrontController@addBasicInfo')->name('add-basic-info');
Route::get('get-basic-info', 'FrontController@getBasicInfo')->name('get-basic-info');


// profile setting route..
Route::get('/role', 'admin\ProfileController@index')->name('role.index');
Route::get('/role/create', 'admin\ProfileController@create')->name('role.create');
Route::post('/role/store', 'admin\ProfileController@store')->name('role.store');
Route::get('/role/{id}/show', 'admin\ProfileController@show')->name('role.show');
Route::get('/role/{id}/edit', 'admin\ProfileController@edit')->name('role.edit');
Route::post('/role/{id}/update', 'admin\ProfileController@update')->name('role.update');



// contact-us controller
Route::get('/contact-us', 'admin\ContactUsController@index')->name('contact-us.index');
// Route::get('/contact-us/create', 'admin\ContactUsController@create')->name('contact-us.create');
Route::post('contact-us/store', 'admin\ContactUsController@store')->name('contact-us.store');
Route::get('contact-us/{id}/edit', 'admin\ContactUsController@edit')->name('contact-us.edit');
Route::post('contact-us/{id}/update', 'admin\ContactUsController@update')->name('contact-us.update');
Route::delete('contact-us/{id}/destroy', 'admin\ContactUsController@destroy')->name('contact-us.destroy');
Route::get('/contactShow/{id}','admin\ContactUsController@getContect_us')->name('contactShow.getContect_us');



 Route::get('/users', 'admin\usersListController@index')->name('users.index');

//Route::post('users/store', 'admin\usersListController@store')->name('users.store');

// Route::get('users/{id}/link', 'admin\usersListController@linkEdit')->name('users.linkEdit');


// Route::get('')

Route::get('resume-exist', 'FrontController@resumeExist')->name('resume-exist');

Route::get('download-pdf', 'FrontController@Downloadpdf')->name('download-pdf');

//Google authentication
Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

// Facebook authentication
Route::get('/login/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('/login/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

