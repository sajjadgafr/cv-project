<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer      = null;
    public $password      = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $password)
    {
        $this->customer     = $customer;
        $this->password      = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $customer    = $this->customer;
        $password    = $this->password;
        return $this->subject('Registration Email')
                    ->view('email.registration_email', compact('customer', 'password'));
    }
}
