<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    protected $guarded = ['id'];
    protected $table = 'certifications';


    public function cvforms(){
    	return $this->hasMany('App\Cvform');
    }
}
