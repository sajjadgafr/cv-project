<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $guarded = ['id'];
    protected $table = 'skills';
     public function cvforms(){
    	return $this->hasMany('App\Cvform');
    }
}
