<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    protected $guarded = ['id'];
    protected $table = 'social_links';


    public function cvforms(){
    	return $this->hasMany('App\Cvform');
    }
}
