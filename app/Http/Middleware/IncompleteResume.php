<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IncompleteResume
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check()){

            // dd(Auth::user()->cvform);
            if(Auth::user()->cvform){
                return $next($request);
            }else{
                return redirect('user/complete-resume');
            }
        }

    }
}
