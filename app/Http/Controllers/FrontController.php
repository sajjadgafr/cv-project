<?php
namespace App\Http\Controllers;
use App\WorkHistory;
use App\Cvform;
use App\Education;
use App\Language;
use App\Skill;
use App\Certification;
use App\Interest;
use App\SocialLink;
use PDF;
use App;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\DB;
use Mail;
use App\User;
use App\Mail\RegistrationEmail;
use Auth;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
       // return view('index_form');
    }

    public function tamplete(){
        return view('templete');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cvForm');
    }

    public function create_contact(){
        // return "hello dear";
         return view('admin.contact-us.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->is_full_time == 'yes'){
            $password = bcrypt($request->password);
            $user = User::create([
                'name'  => $request->firstName.' '.$request->lastName,
                'email'  => $request->confirmation_fields_email,
                'password'  =>  $password
            ]);
        }
         $cvform = new Cvform;
         if($request->file('avatar')){
             $cvImage = time().'.'.request()->avatar->getClientOriginalExtension();
             request()->avatar->move(public_path('uploads/'), $cvImage);
             $cvform->avatar   ='uploads/'.$cvImage;
         }

        $cvform->firstName  = $request->firstName;
        $cvform->lastName   = $request->lastName;
        $cvform->profession = $request->profession;
        $cvform->streetAddress  = $request->streetAddress;
        $cvform->stateProvince  = $request->stateProvince;
        $cvform->city           = $request->city;
        $cvform->zipCode        = $request->zipCode;
        $cvform->phone          = $request->phone;
        $cvform->email          = $request->email;
        $cvform->summary        = $request->summary;
        $cvform->date_of_birth  = $request->dateOfBirth;
        $cvform->marital_status = $request->maritalStatus;
        $cvform->id_card        = $request->idCard;
        $cvform->AdditionalNformation = $request->AdditionalNformation;
        if(@$user){
            $cvform->user_id = $user->id;
        }
        $cvform->save();



        // $interests = implode(",",$_POST['interests']);
        if(!empty($request->jobTitle) ){
         foreach($request->jobTitle as $index=>$number){
            $workHistory = new WorkHistory();
            $workHistory->jobTitle  = $number;
            $workHistory->employer  = $request->employer[$index];
            $workHistory->whCity    = $request->whCity[$index];
            $workHistory->state     = $request->state[$index];
            $workHistory->startDate = $request->startDate[$index];
             isset($workHistory->endtDate) ? $workHistory->endtDate = $request->endtDate[$index] : Null;
            isset($workHistory->currently_working) ? $workHistory->currently_working=$request->currentlyWorking[$index]: Null;
            $workHistory->work_summary=$request->work_summary[$index];
            $workHistory->cvform_id    = $cvform->id;
            $workHistory->save();
         }
        }
            // $FormMulti->chooseLanguage   = $request->chooseLanguage[$index];

        if(!empty($request->schoolName) ){
         foreach ($request->schoolName as $index => $schoolName) {
            $school = new Education();
            $school->schoolName     = $schoolName;
            $school->schoolLocation = $request->schoolLocation[$index];
            $school->degree         = $request->degree[$index];
            $school->studyField     = $request->studyField[$index];
            $school->graduationStart= $request->graduationStart[$index];
            $school->graduationEnd = $request->graduationEnd[$index];
            isset($school->currently_attending) ? $school->currently_attending=$request->currently_attending[$index]: Null;
            isset($school->currently_attending) ? $school->currently_attending=$request->currently_attending[$index]: Null;
            $school->education_summary       = $request->education_summary[$index];
            $school->cvform_id   = $cvform->id;
            $school->save();
         }
        }

        if(!empty($request->chooseLanguage) ){
          foreach ($request->chooseLanguage as $index => $language) {
            $lang = new Language();
            $lang->chooseLanguage     = $language;
            $lang->ProficiencyLanguage = $request->ProficiencyLanguage[$index];
            $lang->cvform_id   = $cvform->id;
            $lang->save();
          }
        }

        if(!empty($request->skill) ){
          foreach ($request->skill as $index => $skills) {
            $skill = new Skill();
            $skill->skill = $skills;
            $skill->skillProgress = $request->skillProgress[$index];
            $skill->cvform_id  = $cvform->id;
            $skill->save();
         }
        }

        if(!empty($request->certificateName) ){
         foreach ($request->certificateName  as $index => $certificat) {
            $certific = new Certification();
            $certific->certificateName  = $certificat;
            $certific->certificDate     = $request->certificDate[$index];

            $certific->certificate_area =$request->certificateArea[$index];
            $certific->certificate_summary =$request->certificate_summary[$index];
            $certific->cvform_id = $cvform->id;
            $certific->save();
         }
        }

        if(!empty($request->interests) ){
          foreach ($request->interests as $index => $interest) {
            $inter = new Interest();
            $inter->interests  = $interest;
            $inter->cvform_id = $cvform->id;
            $inter->save();
          }
        }

         if(!empty($request->sociallinkName) ){
          foreach ($request->sociallinkName as $index => $social) {
            $link = new SocialLink();
            $link->sociallinkName  = $social;
            $link->socialLink      = $request->socialLink[$index];
            $link->cvform_id   = $cvform->id;
            $link->save();
          }
        }
            $lastIid = $cvform->id;
            if(!empty($lastIid)){
              $this->pdfTest_1($lastIid);
            }
            // if(!empty($lastIid)){
            //   $this->pdfTest_2($lastIid);
            // }
            // if(!empty($lastIid)){
            //   $this->pdfTest_3($lastIid);
            // }

            // if (!empty($lastIid)) {
            //    $this->viewPdf($lastIid);
            // }



        return redirect()->route('download.pdfAction',$cvform->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function gotowebiste()
    {
       return view('index');
    }


    public function pdfTest_1($id) {
        $cvform          = Cvform::findOrFail($id);
         $social         = SocialLink::where('cvform_id',$id)->get();
         $language       = Language::where('cvform_id',$id)->get();
         $education      = Education::where('cvform_id',$id)->get();
         $skill          = Skill::where('cvform_id',$id)->get();
         $certificat     = Certification::where('cvform_id',$id)->get();
         $interest       = Interest::where('cvform_id',$id)->get();
         $workHistory    = WorkHistory::where('cvform_id',$id)->get();
        $pdf = PDF::loadView('pdf/test-1', compact('cvform', 'social','language','skill','education','certificat','interest', 'workHistory'));
        return $pdf->stream('my.pdf',array('Attachment'=>0));
    }
    public function pdfTest_2($id) {
        $data = Cvform::find($id);
        $pdf = PDF::loadView('pdf/test-2', compact('data'));
        return $pdf->download('cv-2.pdf');
    }
    public function pdfTest_3($id) {
        $data = Cvform::find($id);
        $pdf = PDF::loadView('pdf/test-3', compact('data'));
        return $pdf->download('cv-3.pdf');
    }



    public function PdfDwonload_1($id){
        $usersInfo = Cvform::findOrFail($id);
        $workHistory = WorkHistory::where('cvform_id',$id)->get();
        $education = Education::where('cvform_id',$id)->get();
        $language = Language::where('cvform_id',$id)->get();
        $skill = Skill::where('cvform_id',$id)->get();
        $certificate = Certification::where('cvform_id',$id)->get();
        $interest = Interest::where('cvform_id',$id)->get();
        $social = SocialLink::where('cvform_id',$id)->get();
        return view('pdf.profile1',compact('usersInfo','workHistory','education','language','skill','certificate','interest','social'));

    }

    public function Downloadpdf(){
        try
        {
            // create the API client instance
            $client = new \Pdfcrowd\HtmlToPdfClient("kashif123", "093afe0f1cff652f175c5446e568ff57");

            // configure the conversion
            // $client->setPageSize("A4");
            // $client->setPageMode("full-screen");
            // $client->setFitWindow(true);
            // $client->setViewportWidth(1100);
            $client->setPageMargins("4mm","4mm", "4mm", "4mm");
            $client->setSmartScalingMode("content-fit");
            // $client->setNoMargins(true);
            // $client->setInitialZoomType("fit-width");

           // run the conversion
            $pdf = $client->convertUrl("http://dailyjobfinders.com/cv/download/10/PdfDwonload_1");

            // set HTTP response headers
            header('Content-Type: application/pdf');
            header('Cache-Control: no-cache');
            header('Accept-Ranges: none');
            header('Content-Disposition: attachment; filename="result.pdf"');

            echo $pdf;
        }
        catch(\Pdfcrowd\Error $why) {
            // report the error
            header('Content-Type: text/plain');
            http_response_code($why->getCode());
            echo "Pdfcrowd Error: {$why}";
        }

    }
//    public function PdfDwonload_2($id){
//        if(!empty($this->pdfTest_2($id))){
//          return $this->pdfTest_2($id);
//        }
//    }
//    public function PdfDwonload_3($id){
//        if(!empty($this->pdfTest_3($id))){
//          return $this->pdfTest_3($id);
//        }
//    }

    public function pdfAction($id){
        $data = Cvform::find($id);
        //return $this->downloadPDF($id);
        return view('viewPdf', compact('data'));
    }

//    public function viewPdf($id){
//        $data = Cvform::find($id);
//         $pdf = PDF::loadView('pdf/test-1', compact('data'));
//         return $pdf-> stream('my.pdf');
//     }

//    public function canclePdf(){
//        return redirect()->route('/');
//    }

    public function addBasicInfo(Request $request){
        // dd($request->all());
        if($request->first_form == 'yes'){
            if($request->edit_form){
                $cvform = Cvform::find($request->cvform_id);
            }else{
                $cvform = new Cvform;
            }
            if($request->file('avatar')){
                $cvImage = time().'.'.request()->avatar->getClientOriginalExtension();
                request()->avatar->move(public_path('uploads/'), $cvImage);
                $cvform->avatar   ='uploads/'.$cvImage;
            }
           $cvform->firstName  = $request->firstName;
           $cvform->lastName   = $request->lastName;
           $cvform->profession = $request->profession;
           $cvform->streetAddress  = $request->streetAddress;
           $cvform->stateProvince  = $request->stateProvince;
           $cvform->city           = $request->city;
           $cvform->zipCode        = $request->zipCode;
           $cvform->phone          = $request->phone;
           $cvform->email          = $request->email;
           $cvform->date_of_birth  = $request->dateOfBirth;
           $cvform->marital_status = $request->maritalStatus;
           $cvform->id_card        = $request->idCard;
           if(Auth::check()){
               $cvform->user_id = Auth::user()->id;
           }
           // if(@$user){
           //     $cvform->user_id = $user->id;
           // }
           $cvform->save();

           if(!empty($request->sociallinkName) ){
            if($request->edit_form && $request->edit_form == 'yes'){
                SocialLink::where('cvform_id', $request->cvform_id)->delete();
            }
                foreach ($request->sociallinkName as $index => $social) {
                    $link = new SocialLink();
                    $link->sociallinkName  = $social;
                    $link->socialLink      = $request->socialLink[$index];
                    $link->cvform_id   = $cvform->id;
                    $link->save();
                }
            }
        }else{
            $cvform = Cvform::find($request->cvform_id);

            //if the user is full time
            if($request->is_full_time == 'yes'){
                $request->validate([
                    'email' => 'required|unique:users',
                ]);
                $password = bcrypt($request->password);
                $user = User::create([
                    'name'  => $request->name,
                    'email'  => $request->email,
                    'password'  =>  $password
                ]);
                $cvform->user_id = $user->id;
                $cvform->save();

                  // send email //
                Mail::to($user->email, $user->name)->send(new RegistrationEmail($user, $request->password));
            }

            if(!empty($request->jobTitle) ){
                // dd($request->currentlyWorking);
                if($request->edit_form && $request->edit_form == 'yes'){
                    WorkHistory::where('cvform_id', $request->cvform_id)->delete();
                }
                foreach($request->jobTitle as $index=>$number){
                $workHistory = new WorkHistory();
                $workHistory->jobTitle  = $number;
                $workHistory->employer  = $request->employer[$index];
                $workHistory->whCity    = $request->whCity[$index];
                $workHistory->state     = $request->state[$index];
                $workHistory->startDate = $request->startDate[$index];
                $workHistory->endtDate = $request->endtDate[$index];
                $workHistory->currently_working=$request->currentlyWorking[$index];
                $workHistory->work_summary=$request->work_summary[$index];
                $workHistory->cvform_id    = $cvform->id;
                $workHistory->save();
                }
            }
                // $FormMulti->chooseLanguage   = $request->chooseLanguage[$index];

            if(!empty($request->schoolName) ){
                if($request->edit_form && $request->edit_form == 'yes'){
                    Education::where('cvform_id', $request->cvform_id)->delete();
                }
                foreach ($request->schoolName as $index => $schoolName) {
                $school = new Education();
                $school->schoolName     = $schoolName;
                $school->schoolLocation = $request->schoolLocation[$index];
                $school->degree         = $request->degree[$index];
                $school->studyField     = $request->studyField[$index];
                $school->graduationStart= $request->graduationStart[$index];
                $school->graduationEnd = $request->graduationEnd[$index];
                // $school->currently_attending =$request->currently_attending[$index];
                $school->currently_attending = $request->currently_attending[$index];
                $school->education_summary       = $request->education_summary[$index];
                $school->cvform_id   = $cvform->id;
                $school->save();
                }
            }

            if(!empty($request->chooseLanguage) ){
                if($request->edit_form && $request->edit_form == 'yes'){
                    Language::where('cvform_id', $request->cvform_id)->delete();
                }
                foreach ($request->chooseLanguage as $index => $language) {
                $lang = new Language();
                $lang->chooseLanguage     = $language;
                $lang->ProficiencyLanguage = $request->ProficiencyLanguage[$index];
                $lang->cvform_id   = $cvform->id;
                $lang->save();
                }
            }

            if(!empty($request->skill) ){
                if($request->edit_form && $request->edit_form == 'yes'){
                    Skill::where('cvform_id', $request->cvform_id)->delete();
                }
                foreach ($request->skill as $index => $skills) {
                $skill = new Skill();
                $skill->skill = $skills;
                $skill->skillProgress = $request->skillProgress[$index];
                $skill->cvform_id  = $cvform->id;
                $skill->save();
                }
            }

            if(!empty($request->certificateName) ){
                if($request->edit_form && $request->edit_form == 'yes'){
                    Certification::where('cvform_id', $request->cvform_id)->delete();
                }
                foreach ($request->certificateName  as $index => $certificat) {
                $certific = new Certification();
                $certific->certificateName  = $certificat;
                $certific->certificDate     = $request->certificDate[$index];

                $certific->certificate_area =$request->certificateArea[$index];
                $certific->certificate_summary =$request->certificate_summary[$index];
                $certific->cvform_id = $cvform->id;
                $certific->save();
                }
            }

            if(!empty($request->interests) ){
                if($request->edit_form && $request->edit_form == 'yes'){
                    Interest::where('cvform_id', $request->cvform_id)->delete();
                }
                foreach ($request->interests as $index => $interest) {
                $inter = new Interest();
                $inter->interests  = $interest;
                $inter->cvform_id = $cvform->id;
                $inter->save();
                }
            }

            //summary
            if($request->summary){
                $cvform->summary        = $request->summary;
            }
            //AdditionalNformation
            if($request->AdditionalNformation){
                $cvform->AdditionalNformation = $request->AdditionalNformation;
            }
            $cvform->save();

            $lastIid = $cvform->id;
                if(!empty($lastIid)){
                    $this->pdfTest_1($lastIid);
            }
        }



            // if(!empty($lastIid)){
            //   $this->pdfTest_2($lastIid);
            // }
            // if(!empty($lastIid)){
            //   $this->pdfTest_3($lastIid);
            // }

            // if (!empty($lastIid)) {
            //    $this->viewPdf($lastIid);
            // }
            if($request->last_form == 'yes'){
                return response()->json([
                    'cvform_id'  => $cvform->id,
                    'last_form' => true,
                    'success' => true
                ]);
                // route('download.pdfAction',$cvform->id);
            }

            return response()->json([
                'success' => true,
                'cvform_id'  => $cvform->id
            ]);
    }

    public function getBasicInfo(Request $request){
        $cvform = Cvform::find($request->cvform_id);
        return response()->json([
            'name' => $cvform->firstName.' '. $cvform->lastName,
            'email' => $cvform->email
        ]);
    }

    public function resumeExist(){
        return view('Front.resume-exist');
    }


}
