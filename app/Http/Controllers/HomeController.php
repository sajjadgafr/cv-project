<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    public function completeResume(){

        if(Auth::check()){
            if(Auth::user()->cvform){
                return redirect('users/singleUser');
            }
        }
        // $countries = Country::get();
     	return view('admin.complete-resume');
    }

}
