<?php

namespace App\Http\Controllers\admin;
use App\ContactUs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $contactUs = ContactUs::all();
         return view('admin.contact-us.index', compact('contactUs'));
        // $this->middleware('auth');
        // if(User::auth != ''){

        // }else{
        //     return redirect()->route('login');
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.contact-us.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

         $contactUs = ContactUs::create([
            'full_name'   => $request->fullname,
            'phone'   => $request->phone,
            'email'   => $request->email,
            'subject'   => $request->subject,
            'message'   => $request->message

        ]);

         return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $contactUs = ContactUs::find($id);
        return view('admin.contact-us.edit', compact('contactUs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // dd($request->all());
           $contactUs = ContactUs::find($id);
            $contactUs->full_name   = $request->fullname;
            $contactUs->phone       = $request->phone;
            $contactUs->email       = $request->email;
            $contactUs->subject     = $request->subject;
            $contactUs->message     = $request->message;

            $contactUs->save();
            return redirect()->route('contact-us.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         ContactUs::where('id',$id)->delete();
         return redirect()->back();

    }


    public function getContect_us($id)
    {
        // $contactUs = ContactUs::find($id);

        $data = ContactUs::find($id);
        return \response()->json([
            'data' => $data,
        ]);
    }
}
