<?php
namespace App\Http\Controllers\admin;
use App\WorkHistory;
use App\Cvform;
use App\Education;
use App\Language;
use App\Skill;
use App\Certification;
use App\Interest;
use App\SocialLink;
use Illuminate\Support\Facades\Auth;
use League\CommonMark\Inline\Element\Link;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class usersListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function cvprofile($id){
         $cvform         = Cvform::findOrFail($id);
         $social         = SocialLink::where('cvform_id',$id)->get();
         $language       = Language::where('cvform_id',$id)->get();
         $education      = Education::where('cvform_id',$id)->get();
         $skill          = Skill::where('cvform_id',$id)->get();
         $certificat     = Certification::where('cvform_id',$id)->get();
         $interest       = Interest::where('cvform_id',$id)->get();
         $workHistory    = WorkHistory::where('cvform_id',$id)->get();
         return view('admin.usersList.viewCvProflle', compact('cvform', 'social','language','skill',
            'education','certificat','interest', 'workHistory'));

         // foreach ($social as $key => $value) {
         //    echo "<br />".$value->sociallinkName;
         // }


     }

    public function index()
    {
        $userList = Cvform::all();
     return view('admin.usersList.userlistings', compact('userList'));
    }
    public function singleUser(){

             $user = Cvform::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
            //  $user = $cvform->user;
            // dd($user);
             return view('admin.usersList.singleUserlist', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // return view('admin.usersList.createUsers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // dd($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $usersEdit = Cvform::findOrFail($id);
        $workHistory = WorkHistory::where('cvform_id',$id)->get();
        $education = Education::where('cvform_id',$id)->get();
        $languageEdit = Language::where('cvform_id',$id)->get();
        $skillEdit = Skill::where('cvform_id',$id)->get();
        $certificatEdit = Certification::where('cvform_id',$id)->get();
        $interestEdit = Interest::where('cvform_id',$id)->get();
        $socialLinkEdit = SocialLink::where('cvform_id',$id)->get();
        // dd($workHistory);
        return view('admin.usersList.editUsers', compact('usersEdit','workHistory','education','languageEdit',
            'skillEdit','certificatEdit','interestEdit','socialLinkEdit'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  dd($request->all());

        $cvform = Cvform::findOrFail($id);

         if($request->file('avatar')){
             $cvImage = time().'.'.request()->avatar->getClientOriginalExtension();
             request()->avatar->move(public_path('uploads/'), $cvImage);
             $cvform->avatar   ='uploads/'.$cvImage;
         }
            $cvform->firstName  = $request->firstName;
            $cvform->lastName   = $request->lastName;
            $cvform->profession = $request->profession;
            $cvform->streetAddress = $request->streetAddress;
            $cvform->stateProvince = $request->stateProvince;
            $cvform->city       = $request->city;
            $cvform->zipCode    = $request->zipCode;
            $cvform->phone      = $request->phone;
            $cvform->email      = $request->email;
            $cvform->summary    = $request->summary;
            $cvform->date_of_birth  = $request->dateOfBirth;
            $cvform->marital_status = $request->maritalStatus;
            $cvform->id_card        = $request->idCard;
            $cvform->AdditionalNformation = $request->AdditionalNformation;
            $cvform->update();

        $workHistory = WorkHistory::where('cvform_id',$cvform->id)->get();
       // DB::table('role_memberships')->where('role_id', $id)->where('MembershipName','PostAdMaxImage')->update(['MembershipValue' => $request->PostAdMaxImage]);
//        foreach($test as $t){
//            print_r($t->whCity);
//        }exit;
         WorkHistory::where('cvform_id',$cvform->id)->delete();
        if(!empty($request->jobTitle) ){
         foreach($request->jobTitle as $index=>$number){
            $workHistory = new WorkHistory();
            $workHistory->jobTitle  = $number;
            $workHistory->employer  = $request->employer[$index];
            $workHistory->whCity    = $request->whCity[$index];
            $workHistory->state     = $request->state[$index];
            $workHistory->startDate = $request->startDate[$index];
             isset($workHistory->endtDate) ? $workHistory->endtDate = $request->endtDate[$index] : Null;
             isset($workHistory->currently_working) ? $workHistory->currently_working=$request->currentlyWorking[$index]: Null;
            $workHistory->work_summary         = $request->work_summary[$index];
            $workHistory->cvform_id    = $cvform->id;
            $workHistory->save();
         }
        }

         //Education::where('cvform_id',$cvform->id)->delete();
       $school = Education::where('cvform_id',$cvform->id);
          if(!empty($request->schoolName) ){
         foreach ($request->schoolName as $index => $schoolName) {
            $school = new Education();
            $school->schoolName     = $schoolName;
            $school->schoolLocation = $request->schoolLocation[$index];
            $school->degree         = $request->degree[$index];
            $school->studyField     = $request->studyField[$index];
            $school->graduationStart= $request->graduationStart[$index];
             $school->graduationEnd = $request->graduationEnd[$index];
            $school->currently_attending = $request->currently_attending[$index];
            $school->education_summary   = $request->education_summary[$index];
            $school->cvform_id = $cvform->id;
            $school->save();
         }
        }

         //Language::where('cvform_id',$cvform->id)->delete();
        $lang = Language::where('cvform_id', $cvform->id);
         if(!empty($request->chooseLanguage) ){
          foreach ($request->chooseLanguage as $index => $language) {
            $lang = new Language();
            $lang->chooseLanguage     = $language;
            $lang->ProficiencyLanguage = $request->ProficiencyLanguage[$index];
            $lang->cvform_id   = $cvform->id;
            $lang->save();
          }
        }

        //Skill::where('cvform_id',$cvform->id)->delete();
       $skill = Skill::where('cvform_id','=',$cvform->id);
        if(!empty($request->skill) ){
          foreach ($request->skill as $index => $skills) {
            $skill = new  Skill();
            $skill->skill = $skills;
            $skill->skillProgress = $request->skillProgress[$index];
            $skill->cvform_id  = $cvform->id;
            $skill->save();
         }
        }

        //Certification::where('cvform_id',$cvform->id)->delete();
       $certific = Certification::where('cvform_id','=',$cvform->id);
        if(!empty($request->certificateName) ){
         foreach ($request->certificateName  as $index => $certificat) {
            $certific = new Certification();
            $certific->certificateName  = $certificat;
            $certific->certificDate     = $request->certificDate[$index];
            $certific->certificate_area =$request->certificateArea[$index];
            $certific->certificate_summary =$request->certificate_summary[$index];
            $certific->cvform_id = $cvform->id;
            $certific->save();
         }
        }

        //Interest::where('cvform_id',$cvform->id)->delete();
        $inter = Interest::where('cvform_id','=',$cvform->id);
         if(!empty($request->interests) ){
          foreach ($request->interests as $index => $interest) {
            $inter = new Interest();
            $inter->interests  = $interest;
            $inter->cvform_id = $cvform->id;
            $inter->save();
          }
        }


         //currently_attendingSocialLink::where('cvform_id',$cvform->id)->delete();
        $link = SocialLink::where('cvform_id','=',$cvform->id);
          if(!empty($request->sociallinkName) ){
          foreach ($request->sociallinkName as $index => $social) {
            $link = new SocialLink();
            $link->sociallinkName  = $social;
            $link->socialLink      = $request->socialLink[$index];
            $link->cvform_id   = $cvform->id;
            $link->save();
          }
        }

        if($request->last_form == 'yes'){
            return response()->json([
                'cvform_id'  => $cvform->id,
                'last_form' => true,
                'success' => true
            ]);
        }

        //  return redirect()->route('users.index');
        return response()->json([
            'success' => true,
            'cvform_id'  => $cvform->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $userList = Cvform::find($id);
        $userList->delete();
        // DB::table("certification")->where("certification_id", $id)->delete();
        Certification::where('cvform_id',$id)->delete();
        WorkHistory::where('cvform_id',$id)->delete();
        Education::where('cvform_id',$id)->delete();
        Language::where('cvform_id',$id)->delete();
        Skill::where('cvform_id',$id)->delete();
        Interest::where('cvform_id',$id)->delete();
        SocialLink::where('cvform_id',$id)->delete();
        return response()->json([
            'success'=> true
        ]);

    }


    public function cv_pdf($id){
        $data = cvform::find($id);
        // dd($data->phone);
        // Send data to the view using loadView function of PDF facade
        $pdf = \PDF::loadView('admin.usersList.userPdf', compact('data'));
        // If you want to store the generated pdf to the server then you can use the store function
        $pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function
        return $pdf->download('cv.pdf');
        }

//        public function linkEdit($id){
//            $data = Link::where('cvform_id', $id)->get();
//            return \response()->json([
//               'data' => $data,
//            ]);
//        }


}
