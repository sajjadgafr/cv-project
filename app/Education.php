<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
	protected $guarded = [];
    protected $table = 'education';

    public function cvforms(){
    	return $this->hasMany('App\Cvform', );
    }

}
