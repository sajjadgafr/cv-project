<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkHistory extends Model
{
    protected $guarded = ['id'];
    protected $table = 'work_histories';

    public function cvforms(){
    	return $this->hasMany('App\Cvform');
    }

}
