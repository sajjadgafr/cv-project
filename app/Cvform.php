<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cvform extends Model
{
    // protected $guarded = ['id'];
    protected $table = 'cvforms';

    public function workhistories()
    {
    	return $this->belongsTo('App\WorkHistory');
    }

    public function educations()
    {
    	return $this->belongsTo('App\Education');
    }

     public function languages()
     {
    	return $this->belongsTo('App\Language');
    }

    public function skills()
    {
    	return $this->belongsTo('App\Skill');
    }

    public function certifications()
    {
    	return $this->belongsTo('App\Certification');
    }

    public function interests()
    {
    	return $this->belongsTo('App\Interest');
    }

     public function sociallinks()
    {
        return $this->belongsTo('App\SocialLink');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
