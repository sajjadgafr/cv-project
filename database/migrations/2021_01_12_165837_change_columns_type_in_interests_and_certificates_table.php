<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsTypeInInterestsAndCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->longText('interests')->change();
        });

        Schema::table('certifications', function (Blueprint $table) {
            $table->longText('certificate_summary')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->string('interests')->change();
        });

        Schema::table('certifications', function (Blueprint $table) {
            $table->string('certificate_summary')->change();
        });
    }
}
