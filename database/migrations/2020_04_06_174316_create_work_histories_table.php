<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_histories', function (Blueprint $table) {
            $table->id();
            $table->string('jobTitle')->nullable();
            $table->string('employer')->nullable();
            $table->string('whCity')->nullable();
            $table->string('state')->nullable();
            $table->date('startDate')->nullable();
            $table->date('endtDate')->nullable();
            $table->string('currently_working')->nullable();
            $table->text('work_summary')->nullable();
            $table->unsignedBigInteger('cvform_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_histories');
    }
}
