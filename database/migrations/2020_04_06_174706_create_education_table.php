<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->id();
            $table->string('schoolName')->nullable();
            $table->string('schoolLocation')->nullable();
            $table->string('degree')->nullable();
            $table->string('studyField')->nullable();
            $table->date('graduationStart')->nullable();
            $table->date('graduationEnd')->nullable();
            $table->string('currently_attending')->nullable();
            $table->text('education_summary')->nullable();
            $table->unsignedBigInteger('cvform_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
