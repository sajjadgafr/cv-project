<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCvformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cvforms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
            $table->text('avatar')->nullable();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('profession')->nullable()->default(null);
            $table->string('streetAddress')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('stateProvince')->nullable()->default(null);
            $table->string('zipCode')->nullable()->default(null);
            $table->string('phone');
            $table->string('email');
            $table->text('summary')->nullable()->default(null);
            $table->text('AdditionalNformation')->nullable()->default(null);
            $table->date('date_of_birth')->nullable()->default(null);
            $table->string('marital_status')->nullable()->default(null);
            $table->string('id_card')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cvforms');
    }
}
