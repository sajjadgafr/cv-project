<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToCvformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cvforms', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable()->after('id_card');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cvforms', function (Blueprint $table) {
            if (Schema::hasColumn('cvforms', 'user_id')) {
                $table->dropColumn('user_id');
            }
        });
    }
}
