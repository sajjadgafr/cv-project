@include('includes.header')
<!-- <banner> -->
<div class="banner d-flex align-items-center" style="background-image: url(assets/img/banner2.jpg);">
  <div class="container py-5">
  <div class="row">
    <div class="col-xl-6 col-lg-7 col-md-8 col-sm-8 col-12">
      <h1 class="text-uppercase font-weight-bold">Creative CV Designs</h1>
      <p class="font-weight-medium">Unique and eye-catching CV Designs that inspire, engage and deliver results.</p>
      <a href="#" class="btn mt-3">Choose Template</a>
    </div>
  </div>
</div>
</div>

    <div class="online-cv-benefits py-5 border-bottom border-color">
      <div class="container mt-3">
        <div class="row">
          <div class="col-md-12 align-items-center mb-4">
              <h2 class="text-center">What are the benefits of online resume maker?</h2>
          </div>
        </div>
        <div class="row">
        <div class="col-md-4 text-center mb-4 cv-benefits">
        <em class="fa fa-car mb-3 rounded-circle"></em>
           <h5>Cover Letter Builder</h5>
           <p>Write a cover letter using the same templates as your resume.</p>
        </div>
        <div class="col-md-4 text-center mb-4 cv-benefits">
        <em class="fa fa-car mb-3 rounded-circle"></em>
           <h5>20+ Best Resume Templates</h5>
           <p>Create a modern and professional resume and cover letter.</p>
        </div>
        <div class="col-md-4 text-center mb-4 cv-benefits">
        <em class="fa fa-car mb-3 rounded-circle"></em>
           <h5>Resume Check</h5>
           <p>Our builder will give you suggestions on how to improve your resume.</p>
        </div>

        <div class="col-md-4 text-center mb-4 cv-benefits">
        <em class="fa fa-car mb-3 rounded-circle"></em>
           <h5>Follow Tips From Expertsr</h5>
           <p>Our experts' tips will show you how to write a resume.</p>
        </div>
        <div class="col-md-4 text-center mb-4 cv-benefits">
        <em class="fa fa-car mb-3 rounded-circle"></em>
           <h5>It's Fast and Easy to Use</h5>
           <p>Write a cover letter using the same templates as your resume.</p>
        </div>
        <div class="col-md-4 text-center mb-4 cv-benefits">
        <em class="fa fa-car mb-3 rounded-circle"></em>
           <h5>Flexible Text Editor</h5>
           <p>Write a cover letter using the same templates as your resume.</p>
        </div>
      </div>
    </div>
  </div>

<section class="about-cv-sections pt-5">
  <div class="container">
          <h1 class="text-center mb-0">Why is the best resume builder online?</h1>
  </div>
  <div class="cv-sect py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
           <img class="img-fluid" src="{{ asset('assets/img/resumeTemplates.png') }}">
        </div>
        <div class="col-md-6">
          <h3>Professional CV Templates</h3>
          <p class="">Choose professional, elegant, creative, or modern resume templates. Zety's resume maker offers 18 templates. You can easily change colors and adapt the layout to any resume format you choose: functional, reverse-chronological, or combination.</p>
        </div>
      </div>
    </div>
 </div>
  <div class="cv-sect py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
           <h3>Tips From Recruiters</h3>
          <p class="">Choose professional, elegant, creative, or modern resume templates. Zety's resume maker offers 18 templates. You can easily change colors and adapt the layout to any resume format you choose: functional, reverse-chronological, or combination.</p>
        </div>
         <div class="col-md-6">
          <img class="img-fluid" src="assets/img/expert-insights.png">
          </div>
      </div>
    </div>
  </div>
  <div class="cv-sect py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
         <img class="img-fluid" src="assets/img/edit-your-resume-as-you-like@1x.png">
        </div>
         <div class="col-md-6">
             <h3>Tips From Recruiters</h3>
          <p class="">Choose professional, elegant, creative, or modern resume templates. Zety's resume maker offers 18 templates. You can easily change colors and adapt the layout to any resume format you choose: functional, reverse-chronological, or combination.</p>
          </div>
      </div>
    </div>
  </div>
  <div class="cv-sect py-5">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6">
           <h3>Tips From Recruiters</h3>
          <p class="">Choose professional, elegant, creative, or modern resume templates. Zety's resume maker offers 18 templates. You can easily change colors and adapt the layout to any resume format you choose: functional, reverse-chronological, or combination.</p>
        </div>
         <div class="col-md-6">
          <img class="img-fluid" src="assets/img/matchingCoverLetter.png">
          </div>
      </div>
    </div>
  </div>
   </section>
<!--   <div class="professional-cv py-5 sect-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1 class="py-4">Write your professional resume online.</h1>
          <p class="py-4">Download with a single click. Land that dream job.</p>
             <figure class="mb-0">
               <img class="img-fluid mt-5" src="assets/img/carousel-resume-templates-1093@1_25x.jpg">
              <figcaption>
                <a href="#" class="btn btn-danger">GO TO RESUME BUILDER</a>
              </figcaption>

             </figure>

        </div>
      </div>
    </div>
  </div> -->


<div class="professional-cv sect-bg py-5">
  <div class="container py-4">
    <div class="row">
      <div class=" col-12 text-center">
        <h3 class="mb-3">Try professional resume builder now</h3>
        <a href="#"class="btn">Land your dream job now</a>
      </div>
    </div>
  </div>
</div>


<!-- section-7 start here -->
<div class="container my-5">
  <div class="row align-items-center">
      <div class="col-12 col-carousel">
  <div class="owl-carousel owl-theme carousel-main">
    <div class="mx-2 item text-center ">
      <img class="img-fluid d-block img-fluid mx-auto mb-4" src="assets/img/Download.png">
      <p class=" quote  ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book..</p>
       <div class="mt-5">
       <h5 class="font-weight-bold">Williamson</h5>
       <span class="post"><strong>Web Developer</strong></span>
     </div>
    </div>
     <div class="mx-2 item text-center" >
      <img class="img-fluid d-block img-fluid mx-auto mb-4" src="assets/img/Download.png">
      <p class="quote">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
       <div class="mt-5">
         <h5 class="font-weight-bold">Williamson</h5>
         <span class="post"><strong>Web Developer</strong></span>
       </div>
       </div>
      <div class="mx-2 item text-center" >
             <img class="img-fluid d-block img-fluid mx-auto mb-4" src="assets/img/Download.png">
        <p class=" quote">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
         <div class="mt-5">
             <h5 class="font-weight-bold">Williamson</h5>
             <span class="post"><strong>Web Developer</strong></span>
           </div>
        </div>
       <div class="mx-2 item text-center">
          <img class="img-fluid d-block img-fluid mx-auto mb-4" src="assets/img/Download.png">
        <p class=" quote">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        <div class="mt-5">
           <h5 class="font-weight-bold">Williamson</h5>
           <span class="post"><strong>Web Developer</strong></span>
         </div>
         </div>
  </div>
  </div>
</div>
</div>

@include('includes.footer')
