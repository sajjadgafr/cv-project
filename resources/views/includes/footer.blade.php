<footer class="bg-light footer pt-5">
  <div class="container">
    <div class="row justify-content-between"> 
      <div class="col-md-8">
        <ul class="list-unstyled mb-0">
          <li class="list-inline-item">
            <a href="#">About</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Contact</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Terms of Service</a>
          </li>
          <li class="list-inline-item"><a href="#">Privacy Policy</a>
          </li>
        </ul>
      </div>
      <div class="col-md-4 social-links text-right">
        <ul class="list-unstyled mb-0 d-inline-block">
          <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="copyright border-top py-4 text-center mt-5">
    <div class="container">
        <p class="mb-0">© 2020 Works Limited. All Rights Reserved.</p>
    </div>
  </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>
<script src="{{ asset('/assets/js/bootstrap-datepicker.min.js') }}" ></script>

<script type="text/javascript">

 $(".owl-carousel").owlCarousel({});

  $('.carousel-main').owlCarousel({
  items: 1,
  loop: true,
  autoplay: true,
  autoplayTimeout: 15000,
  dots:true,

})



</script>
</body>
</html>