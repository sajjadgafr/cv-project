
@include('includes.head')

<header class="border-bottom header py-3">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-2 col-lg-3 logo d-md-block d-none">
                <a href="{{ route('webiste.gotowebiste') }}">
                  <img class="img-fluid" src="{{ asset('/assets/img/logo.png') }}">
                  <!-- <strong style="font-size: 40px; line-height: 1;">Logo</strong>  -->
                </a>
            </div>
            <nav class="col-md-7 col-12 navbar navbar-expand-md py-0 w-100">
                      <a class="navbar-brand d-md-none p-0" href="#">
                       <img class="img-fluid" src="{{ asset('/assets/img/logo.png') }}">
                      </a>

                      <!-- Toggler/collapsibe Button -->
                      <button class="navbar-toggler fa fa-bars p-0" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                        <!-- <span class="navbar-toggler-icon"></span> -->
                      </button>

                      <!-- Navbar links -->
                      <div class="collapse justify-content-end navbar-collapse" id="collapsibleNavbar">
                        <ul class="navbar-nav mt-md-0 mt-3">
                          <li class="nav-item">
                            <a class="nav-link" href="#">Tools</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Resume </a>
                          </li>
                          <li class="nav-item">
                            @if(Auth::check())
                                @php
                                    $cv = \App\CVform::where('user_id', Auth::user()->id)->first();
                                    $route = $cv==null?route('form.create'):route('resume-exist') ;
                                @endphp
                                <a class="nav-link" href="{{ $route }}">CV</a>
                              @else
                              <a class="nav-link" href="{{ route('form.create') }}">CV</a>
                            @endif

                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Cover Letter</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('contact.create_contact') }}">Contact Us</a>
                          </li>
                           <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                          </li>
                        </ul>
                      </div>
                    </nav>
            <div class="col-sm-2 text-right">
                <a href="{{ route('login') }}" class="btn px-4">My Account</a>
            </div>
        </div>
    </div>
	</header>
