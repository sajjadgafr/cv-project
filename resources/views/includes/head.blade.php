<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CV Builder</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

{{-- Toastr Plugin --}}
<link href="{{asset('assets/front/css/toastr.min.css')}}" rel="stylesheet">

{{-- SweetAlert2 Plugin --}}
<link href="{{asset('assets/front/css/sweetalert2.min.css')}}" rel="stylesheet">

{{-- custom style --}}
<link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>
 --><script src="{{ asset('/assets/js/bootstrap-datepicker.min.js') }}" ></script>

{{-- Toastr Plugin --}}
<script  src="{{asset('assets/front/js/toastr.min.js')}}"></script>

{{-- sweetalert2 plugin --}}
<script  src="{{asset('assets/front/js/sweetalert2.all.min.js')}}"></script>
<script  src="{{asset('assets/front/js/sweetalert2.min.js')}}"></script>

<script>

    // $(document).ready(function(){
    //     const Swal = require('sweetalert2');
    // });

</script>
</head>
<body>

