@include('includes.header')

<section class="form-wizard-section py-5" style="background: #f8f8f8;">
<div class="container">
    {{-- row --}}
    @php
        $cvform = \App\CVform::where('user_id', Auth::user()->id)->first();
    @endphp
    <div class="row justify-content-center">
     <div class="card border-primary mb-3 mr-2" style="max-width: 18rem;">
        <a href="{{ url('/users/'.$cvform->id.'/edit') }}">
        {{-- <div class="card-header">Header</div> --}}
        <div class="card-body text-primary">
          <h5 class="card-title">Update Resume</h5>
          <p class="card-text">If you want to make any changes then click here</p>
        </div>
        </a>
      </div>
      <div class="card border-secondary mb-3 mr-2" style="max-width: 18rem;">
        <a href="{{ url('users/'.$cvform->id.'/cvprofile') }}">
        {{-- <div class="card-header">Header</div> --}}
        <div class="card-body text-secondary">
          <h5 class="card-title">Preview </h5>
          <p class="card-text">If you want tProfileo profile changes then click here</p>
        </div>
        </a>
      </div>
      <div class="card border-success mb-3 mr-2" style="max-width: 18rem;">
        <a href="{{ url('/cv/'.$cvform->id.'/pdf') }}">
        {{-- <div class="card-header">Header</div> --}}
        <div class="card-body text-success">
          <h5 class="card-title">Download Resume</h5>
          <p class="card-text">If you want to download resume then click here.</p>
        </div>
        </a>
      </div>
      <div class="card border-danger mb-3 mr-2" style="max-width: 18rem;">
        @php
            @$cvform_id = \App\CVform::where('user_id', $cvform->id)->first()->id;
        @endphp
        <a href="javascript:void(0);" class="delete-btn" data-id="{{ @$cvform_id }}">
        {{-- <div class="card-header">Header</div> --}}
        <div class="card-body text-danger">
          <h5 class="card-title">Delete Resume</h5>
          <p class="card-text">If you want to delete resume click here, You can't be undone</p>
        </div>
        </a>
      </div>
    </div>

</div>
  </div>
</section>

  </div>

  <script>
      $(document).ready(function(){
        $(document).on('click', '.delete-btn', function(){

var id = "{{ $cvform->id }}";
const swalWithBootstrapButtons = Swal.mixin({
customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
},
buttonsStyling: false
})

swalWithBootstrapButtons.fire({
title: 'Are you sure?',
text: "You won't be able to revert this!",
icon: 'warning',
showCancelButton: true,
confirmButtonText: 'Yes, delete it!',
cancelButtonText: 'No, cancel!',
reverseButtons: true
}).then((result) => {
if (result.isConfirmed) {
    $.ajax({
        method: 'get',
        url: "{{ route('users.destroy') }}",
        dataType: 'json',
        data: {id:id},
        beforeSend: function(){
            $('.delete-btn').attr('disabled', true);
        },
        success: function (response) {
            $('.delete-btn').attr('disabled', false);
            swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your resume has been deleted.',
                'success'
            ).then((result) => {
            if (result.isConfirmed) {
                window.location.href = "{{ route('form.create') }}";
            }

                window.location.href = "{{ route('form.create') }}";

            })

        },
        error: function (request, status, error) {
                toastr.error('Error!', "Something went wrong. Please try again later. If the issue persists, contact support.",{"positionClass": "toast-bottom-right"});
        }
    });

} else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
) {
    swalWithBootstrapButtons.fire(
    'Cancelled',
    'Your resume is safe :)',
    'error'
    )
}
});
});
      });
  </script>

@include('includes.footer')
