@include('includes.header')

<section class="form-wizard-section py-5" style="background: #f8f8f8;">
<div class="container">
  <div class="row">
    <div class="col-12">
      <ul id="progressbar" class="d-flex justify-content-between mb-4 pb-2">
          <li class="active">Basic Info</li>
          <li>Work History</li>
          <li>Education</li>
          <li>Languages</li>
          <li>Skills</li>
          <li>Summary</li>
        </ul>
    </div>


  <div class="col-12">
    {{-- <form method="POST" action="{{ route('form.store') }}"  enctype="multipart/form-data" id="msform" class="sign-form">
        {{ csrf_field() }} --}}
    <!-- Contact History Fields -->

    <fieldset id="fieldset-1">
        <form action="" class="basicInfoForm">
            @csrf
            <input type="hidden" name="first_form" value="yes">
           <div class="form-title-col mb-4">
            <h3 class="form-title">What’s the best way for employers to contact you?</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>
      <div class="basic-info-row">
        <div class="row">

        <div class="form-group col-12 text-center">
          <!-- <label for="Avatar" >Avatar</label> -->
          <!-- <input type="file" class="form-control " name="avatar" > -->
          <div class="file-field">
            <div class="mb-4">
              <img src="https://mdbootstrap.com/img/Photos/Others/placeholder-avatar.jpg"
                class="rounded-circle z-depth-1-half avatar-pic" alt="example placeholder avatar" style="width: 150px;">
            </div>
            <div class="d-flex justify-content-center">
              <div class="btn btn-mdb-color btn-rounded float-left">
                <span>Add Avatar</span>
                <input type="file" name="avatar" id="imgInp">
              </div>
            </div>
          </div>
        </div>







        <div class="form-group col-6">
          <label for="firstName">First Name <sup>*</sup></label>
          <input type="text" class="form-control required-field firstName" name="firstName">
        </div>
        <div class="form-group col-6">
          <label for="lastName">Last Name <sup>*</sup></label>
          <input type="text" class="form-control required-field lastName" name="lastName">
        </div>
        <div class="form-group col-6">
          <label for="Profession">Profession</label>
          <input type="text" class="form-control" name="profession">
        </div>
        <div class="form-group col-6">
          <label for="Street Address">Street Address</label>
          <input type="text" class="form-control" name="streetAddress">
        </div>
        <div class="form-group col-6">
          <label for="City">City</label>
          <input type="text" class="form-control" name="city" >
        </div>
        <div class="form-group col-6">
          <label for="Country/State">Country/State</label>
          <input type="text" class="form-control" name="stateProvince">
        </div>
        <div class="form-group col-6">
          <label for="Zip Code">Zip Code</label>
          <input type="tel" class="form-control" name="zipCode">
        </div>
        <div class="form-group col-6">
          <label for="Date of Birth">Date of Birth</label>
          <input type="date" class="form-control" name="dateOfBirth">
        </div>
        <div class="form-group col-6">
          <label for="Marital Status">Marital Status</label>
          <select class="form-control" name="maritalStatus">
            <option >Select One</option>
            <option value="Single">Single</option>
            <option value="Married">Married</option>
            <option value="Separated">Separated</option>
            <option value="Divorced">Divorced</option>
            <option value="Widowed ">Widowed</option>
          </select>
        </div>
        <div class="form-group col-6">
          <label for="ID card Number">ID card Number (Optional)</label>
          <input type="tel" class="form-control" name="idCard">
        </div>
        <div class="form-group col-6">
          <label for="Phone">Phone <sup>*</sup></label>
          <input type="tel" class="form-control required-field" name="phone">
        </div>
         <div class="form-group col-6">
          <label for="Email Address">Email Address <sup>*</sup></label>
          <input type="email" class="form-control required-field email" name="email">
        </div>
        <!-- <div class="form-group col-6">
          <label for="Avatar">Avatar</label>
          <input type="file" class="form-control" name="avatar" >
        </div> -->
           </div>
          </div>
           <div class="text-primary form-group add-social-link">
             <em class="fa fa-plus-square mr-2"></em> Add Social Link
           </div>
            <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
              <button type="submit" class="btn btn-next basic-info-next-btn ml-auto">Next</button>
            </div>
        </form>
        </fieldset>

        <!-- Work History Fields -->
        <fieldset id="fieldset-2">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Now, let’s fill out your work history</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>


           <div class="add-job">
          <div class="row">
          <div class="form-group col-6">
          <label for="Job Title">Job Title</label>
          <input type="text" class="form-control" name="jobTitle[]">
        </div>

        <div class="form-group col-6">
          <label for="Area of Work">Area of Work</label>
          <input type="text" class="form-control" name="employer[]">
        </div>

        <div class="form-group col-6">
          <label for="City">City</label>
          <input type="text" class="form-control" name="whCity[]">
        </div>

        <div class="form-group col-6">
          <label for="State">State</label>
          <input type="text" class="form-control" name="state[]">
        </div>
        <div class="form-group col-6">
          <label for="Start Date">Start Date</label>
          <input type="date" class="form-control" id="startDate" name="startDate[]">
        </div>
        <div class="form-group col-6">
          <label for="End Date">End Date</label>
          <input type="date" class="form-control" id="endtDate" name="endtDate[]">
          <div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2">
              <input type="checkbox"  name="currentlyWorking[]" id="currentlyWorking" class="custom-control-input" value="Present">
              <label for="currentlyWorking"  class="custom-control-label">I currently working here</label></div>
        </div>
        <div class="form-group col-12">
          <label for="End Date">Work Summary</label>
          <textarea  class="form-control"  name="work_summary[]" rows="5"></textarea>
        </div>
       </div>
        </div>
              <div class="text-primary form-group add-more-job"><em class="fa fa-plus-square mr-2"></em> Add More</div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- Education Fields Start here -->
        <fieldset id="fieldset-3">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Great, let’s work on your education</h3>
            <!-- <p class="form-sub-title">We suggest including an email and phone number.</p> -->
           </div>
             <div class="row-education">
           <div class="row">
            <div class="form-group col-6">
            <label for="End Date">School Name</label>
            <input type="text" class="form-control" name="schoolName[]">
          </div>
          <div class="form-group col-6">
            <label for="End Date">School Location</label>
            <input type="text" class="form-control" name="schoolLocation[]">
          </div>
          <div class="form-group col-6">
            <label for="End Date">Degree</label>
          <select class="form-control" data-live-search="true" name="degree[]">
            <option data-tokens="Ph.D">Ph.D</option>
            <option data-tokens="Arts">Arts</option>
            <option data-tokens="IT">IT</option>
          </select>

          </div>
          <div class="form-group col-6">
            <label for="End Date">Field of Study</label>
            <input type="text" class="form-control" name="studyField[]">
          </div>
          <div class="form-group col-6">
            <label for="End Date">Graduation Start Date</label>
            <input type="date" class="form-control" id="edu_start_date" name="graduationStart[]">
          </div>
          <div class="form-group col-6">
            <label for="End Date">Graduation End Date</label>
            <input type="date" class="form-control" id="edu_end_date" name="graduationEnd[]">
            <div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2">
              <input type="checkbox" name="currently_attending[]" id="currentlyAttending" class="custom-control-input"  value="Present">
              <label for="currentlyAttending" class="custom-control-label">I currently attend here</label></div>
          </div>
          <div class="form-group col-12">
            <label for="End Date">Education Summary</label>
            <textarea  class="form-control"  name="education_summary[]" rows="5"></textarea>
          </div>

           </div>
             </div>
             <div class="text-primary form-group add-more-education"><em class="fa fa-plus-square mr-2"></em> Add More</div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <!-- Languages Fields Start here -->
        <fieldset id="fieldset-4">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Add Language</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>
            <div class="language-row">

           <div class="row">
            <div class="form-group col-6">
            <label for="End Date">Choose Language</label>
            <!-- <select class="form-control " name="chooseLanguage[]" id="langName">
                            <option value="">Choose Language</option>
                                <option value="555"> African </option>
                                <option value="557"> Albanian </option>
                                <option value="553"> Arabic </option>
                                <option value="559"> Armenian </option>
                                <option value="1167"> Balochi </option>
                                <option value="1338"> Chinese </option>
                            </select> -->
            <select data-placeholder="Choose a Language..." class="form-control chooseLanguage" name="chooseLanguage[]" id="langName"><option value="">Choose Language</option><option value="Afrikaans">Afrikaans</option><option value="Albanian">Albanian</option><option value="Arabic">Arabic</option><option value="Armenian">Armenian</option><option value="Basque">Basque</option><option value="Bengali">Bengali</option><option value="Bulgarian">Bulgarian</option><option value="Catalan">Catalan</option><option value="Cambodian">Cambodian</option><option value="Chinese (Mandarin)">Chinese (Mandarin)</option><option value="Croatian">Croatian</option><option value="Czech">Czech</option><option value="Danish">Danish</option><option value="Dutch">Dutch</option><option value="English">English</option><option value="Estonian">Estonian</option><option value="Fiji">Fiji</option><option value="Finnish">Finnish</option><option value="French">French</option><option value="Georgian">Georgian</option><option value="German">German</option><option value="Greek">Greek</option><option value="Gujarati">Gujarati</option><option value="Hebrew">Hebrew</option><option value="Hindi">Hindi</option><option value="Hungarian">Hungarian</option><option value="Icelandic">Icelandic</option><option value="Indonesian">Indonesian</option><option value="Irish">Irish</option><option value="Italian">Italian</option><option value="Japanese">Japanese</option><option value="Javanese">Javanese</option><option value="Korean">Korean</option><option value="Latin">Latin</option><option value="Latvian">Latvian</option><option value="Lithuanian">Lithuanian</option><option value="Macedonian">Macedonian</option><option value="Malay">Malay</option><option value="Malayalam">Malayalam</option><option value="Maltese">Maltese</option><option value="Maori">Maori</option><option value="Marathi">Marathi</option><option value="Mongolian">Mongolian</option><option value="Nepali">Nepali</option><option value="Norwegian">Norwegian</option><option value="Persian">Persian</option><option value="Polish">Polish</option><option value="Portuguese">Portuguese</option><option value="Punjabi">Punjabi</option><option value="Quechua">Quechua</option><option value="Romanian">Romanian</option><option value="Russian">Russian</option><option value="Samoan">Samoan</option><option value="Serbian">Serbian</option><option value="Slovak">Slovak</option><option value="Slovenian">Slovenian</option><option value="Spanish">Spanish</option><option value="Swahili">Swahili</option><option value="Swedish ">Swedish</option><option value="Tamil">Tamil</option><option value="Tatar">Tatar</option><option value="Telugu">Telugu</option><option value="Thai">Thai</option><option value="Tibetan">Tibetan</option><option value="Tonga">Tonga</option><option value="Turkish">Turkish</option><option value="Ukrainian">Ukrainian</option><option value="Urdu">Urdu</option><option value="Uzbek">Uzbek</option><option value="Vietnamese">Vietnamese</option><option value="Welsh">Welsh</option><option value="Xhosa">Xhosa</option> </select>


          </div>
          <div class="form-group col-6">
            <label for="myRange">Proficiency with this language*</label>
          <select class="form-control languageProficiency" name="ProficiencyLanguage[]" id="langRate">
                            <option value="">Select One</option>
                            <option value="Beginner">Beginner</option>
                            <option value="Intermediate">Intermediate</option>
                            <option value="Expert">Expert</option>
                        </select>
          </div>
           </div>
          </div>
           <div class="text-primary form-group add-more-languages"><em class="fa fa-plus-square mr-2"></em> Add More Skills</div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <!-- Skills Fields Start here -->
        <fieldset id="fieldset-5">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Add Skills</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>
            <div class="skills-row">
           <div class="row">
            <div class="form-group col-6">
            <label for="End Date">Skill</label>
            <input type="text" class="form-control" name="skill[]">
          </div>
          <div class="form-group col-6">
            <label for="myRange">Skill Progress</label>
          <div class="slidecontainer d-flex justify-content-between align-items-center">
              <input type="range" min="0" max="100" value="50" name="skillProgress[]" class="custom-range myRange" id="myRange">
              <div class="ml-3 text-nowrap">Value: <span id="demo" class="d-inline-block font-weight-bold mr-1 myRangeVal"></span><em class="fa fa-percent"></em></div>
          </div>
          </div>
           </div>
          </div>
           <div class="text-primary form-group add-more-skills"><em class="fa fa-plus-square mr-2"></em> Add More Skills</div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <!-- Summary Fields Start here -->
        <fieldset id="fieldset-6">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Briefly tell us about your background</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>
            <div class="row">
              <div class="form-group col-12">
            <label for="End Date">Summary</label>
            <textarea class="form-control" name="summary" rows="5"></textarea>
          </div>
            </div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <!-- <fieldset id="fieldset-7">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Do you have anything else to add?</h3>
            <p class="form-sub-title">These sections are optional.</p>
           </div>
            <div class="row">
              <div class="form-group col-sm-4 col-12">
          <div class="custom-control custom-checkbox customCheckbox ">
              <input type="checkbox" class="custom-control-input" id="AdditionalInfo" name="example1">
              <label class="custom-control-label" for="AdditionalInfo">Additional Information</label>
            </div>
          </div>
          <div class="form-group col-sm-4 col-12">
          <div class="custom-control custom-checkbox customCheckbox ">
              <input type="checkbox" class="custom-control-input" id="Certifications" name="example1">
              <label class="custom-control-label" for="Certifications">Certifications</label>
            </div>
          </div>
          <div class="form-group col-sm-4 col-12">
          <div class="custom-control custom-checkbox customCheckbox ">
              <input type="checkbox" class="custom-control-input" id="Interests" name="example1">
              <label class="custom-control-label" for="Interests">Interests</label>
            </div>
          </div>
            </div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="button" class="btn btn-next">Save</button>
                <button type="submit" class="btn submitbtn">Submit</button>
              </div>

        </fieldset> -->
        <!-- // -->


        <fieldset id="fieldset-8">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Additional information</h3>
            <p class="form-sub-title">Add anything else you want employers to know.</p>
           </div>
            <div class="row">
              <div class="form-group col-12">
            <label for="End Date">Additional information</label>
            <textarea class="form-control" name="AdditionalNformation" rows="5"></textarea>
          </div>
            </div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
         <fieldset id="fieldset-9">
             <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">What certifications do you have?</h3>
            <p class="form-sub-title">Add anything else you want employers to know.</p>
           </div>


           <div class="certificate-row">
            <div class="row form-row">
              <div class="form-group col-4">
            <label for="End Date">Select Date</label>
            <input type="date" class="form-control" id="certificDate" name="certificDate[]">
          </div>
          <div class="form-group col-4">
            <label for="End Date">Certificate Name</label>
            <input type="text" class="form-control" name="certificateName[]">
          </div>
          <div class="form-group col-4">
            <label for="End Date">Certification Area</label>
            <input type="text" class="form-control" name="certificateArea[]">
          </div>
          <div class="form-group col-12"><label for="End Date">Certificate Summary</label><textarea  class="form-control"  name="certificate_summary[]" rows="5"></textarea></div>
            </div>
          </div>
          <div class="text-primary form-group add-more-certificate"><em class="fa fa-plus-square mr-2"></em> Add More</div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
         <fieldset>
             <form action="" class="basicInfoForm" id="lastForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
          <div class="form-title-col mb-4">
            <h3 class="form-title">What interests do you have?</h3>
            <p class="form-sub-title">Add anything else you want employers to know.</p>
           </div>


           <div class="interests-row">
            <div class="row">
          <div class="form-group col-6">
            <label for="End Date">interests</label>
            <input type="text" class="form-control" name="interests[]">
          </div>
            </div>
          </div>

          <input type="hidden" name="last_form" value="">
          {{-- <input type="hidden" name="confirmation_fields_email" id="confirmation_fields_email" value=""> --}}
          <input type="hidden" name="email" id="confirmation_email" value="">
          <input type="hidden" name="name" id="confirmation_name" value="">
          <input type="hidden" name="password" id="confirmation_password" value="">
          <input type="hidden" name="is_full_time" id="is_full_time" value="">
          <div class="text-primary form-group add-more-interests"><em class="fa fa-plus-square mr-2"></em> Add More</div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn submitbtn submitLastForm">Done</button>
              </div>
            </form>
        </fieldset>
  {{-- </form> --}}
   </div>
</div>
  </div>
</section>


<!--  Register Start Here -->
<div class="modal" id="registerModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered parcelpop">
      <div class="modal-content">

        <div class="modal-body text-center">
          <h3 class="text-capitalize fontmed"><span id="modal_heading"></span> Confirmation Dialogue</h3>
          <div class="mt-4">
          {{-- <form action="" id="confirmationDailogueModal"> --}}
            <p class="font-weight-bold"> <span style="color:red;">Note :</span>  Please check one of the following</p>
            <label class="radio-inline">
                <input type="radio" class="m-1" value="one_time" name="optradio">One Time CV
              </label>
              <label class="radio-inline">
                <input type="radio" class="m-1" value="full_time" name="optradio">Full Time CV
              </label>
            <div class="basic-info d-none">
                <div class="form-row m-2">
                    <label for="name" class="font-weight-bold">Name: </label>
                    <input type="text" value="" name="name" class="form-control confirm-dailogue-name">
                </div>
                <div class="form-row m-2">
                    <label for="name" class="font-weight-bold">Email: </label>
                    <input type="email" name="email" class="form-control confirm-dailogue-email">
                </div>
                <div class="form-row m-2 position-relative">
                    <label for="name" class="font-weight-bold">Password: </label>
                    <input type="password" name="password" id="password-field" class="form-control confirm-dailogue-password">
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password mr-2 position-absolute" style="top: 60%;right:7px;"></span>
                </div>
                <div class="form-row m-2 position-relative">
                    <label for="name" class="font-weight-bold">Confirm Password: </label>
                    <input type="password" id="confirm-password" name="password_confirmation" class="form-control confirm-dailogue-c-password">
                    <span toggle="#confirm-password" class="fa fa-fw fa-eye field-icon toggle-confirm-password mr-2 position-absolute" style="top: 60%;right:7px;"></span>
                </div>
                 <input type="hidden" name="is_full_time" value="1">
               </div>
            </div>

            <div class="form-submit mt-5">
              <input type="submit" value="Submit" class="btn btn-bg save-btn">
              {{-- <input type="reset" value="close" data-dismiss="modal"  class="btn btn-danger close-btn"> --}}
            </div>
        {{-- </form> --}}
         </div>
        </div>
      </div>
    </div>
  </div>


<!-- Register Modal End Here -->


<script type="text/javascript">

//$('#startDate'+datecount1111).datepicker({});
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.avatar-pic').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#imgInp").change(function() {
  readURL(this);
});

var slider = $('.myRange').val();
var output = $('.myRangeVal').text(slider);

$(document).on('change', '.myRange', function(){
  $(this).siblings('div').children('.myRangeVal').html( $(this).val() );
})

$(function(){
$('.add-social-link').click(function(){
  $('.basic-info-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">Social Website</label><select class="form-control" name="sociallinkName[]"><option class="">Select One</option><option class="">twitter</option><option class="">facebook</option><option class="">linkedin</option></select></div><div class="form-group col-6"><label for="myRange">Social Link</label><input type="text" class="form-control" name="socialLink[]" value=""></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });

$('.add-more-skills').click(function(){
  $('.skills-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">Skill</label><input type="text" class="form-control" name="skill[]"></div><div class="form-group col-6"><label for="myRange">Skill Progress</label><div class="slidecontainer d-flex justify-content-between align-items-center"><input type="range" min="0" max="100" value="50" name="skillProgress[]" class="custom-range myRange" id="myRange"> <div class="ml-3 text-nowrap">Value: <span id="demo" class="d-inline-block font-weight-bold mr-1 myRangeVal">50</span><em class="fa fa-percent"></em></div></div></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });
$('.add-more-languages').click(function(){
  $('.language-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">Choose Language</label><select data-placeholder="Choose a Language..." class="form-control chooseLanguage" name="chooseLanguage[]" id="langName"><option value="">Choose Language</option><option value="Afrikaans">Afrikaans</option><option value="Albanian">Albanian</option><option value="Arabic">Arabic</option><option value="Armenian">Armenian</option><option value="Basque">Basque</option><option value="Bengali">Bengali</option><option value="Bulgarian">Bulgarian</option><option value="Catalan">Catalan</option><option value="Cambodian">Cambodian</option><option value="Chinese (Mandarin)">Chinese (Mandarin)</option><option value="Croatian">Croatian</option><option value="Czech">Czech</option><option value="Danish">Danish</option><option value="Dutch">Dutch</option><option value="English">English</option><option value="Estonian">Estonian</option><option value="Fiji">Fiji</option><option value="Finnish">Finnish</option><option value="French">French</option><option value="Georgian">Georgian</option><option value="German">German</option><option value="Greek">Greek</option><option value="Gujarati">Gujarati</option><option value="Hebrew">Hebrew</option><option value="Hindi">Hindi</option><option value="Hungarian">Hungarian</option><option value="Icelandic">Icelandic</option><option value="Indonesian">Indonesian</option><option value="Irish">Irish</option><option value="Italian">Italian</option><option value="Japanese">Japanese</option><option value="Javanese">Javanese</option><option value="Korean">Korean</option><option value="Latin">Latin</option><option value="Latvian">Latvian</option><option value="Lithuanian">Lithuanian</option><option value="Macedonian">Macedonian</option><option value="Malay">Malay</option><option value="Malayalam">Malayalam</option><option value="Maltese">Maltese</option><option value="Maori">Maori</option><option value="Marathi">Marathi</option><option value="Mongolian">Mongolian</option><option value="Nepali">Nepali</option><option value="Norwegian">Norwegian</option><option value="Persian">Persian</option><option value="Polish">Polish</option><option value="Portuguese">Portuguese</option><option value="Punjabi">Punjabi</option><option value="Quechua">Quechua</option><option value="Romanian">Romanian</option><option value="Russian">Russian</option><option value="Samoan">Samoan</option><option value="Serbian">Serbian</option><option value="Slovak">Slovak</option><option value="Slovenian">Slovenian</option><option value="Spanish">Spanish</option><option value="Swahili">Swahili</option><option value="Swedish ">Swedish</option><option value="Tamil">Tamil</option><option value="Tatar">Tatar</option><option value="Telugu">Telugu</option><option value="Thai">Thai</option><option value="Tibetan">Tibetan</option><option value="Tonga">Tonga</option><option value="Turkish">Turkish</option><option value="Ukrainian">Ukrainian</option><option value="Urdu">Urdu</option><option value="Uzbek">Uzbek</option><option value="Vietnamese">Vietnamese</option><option value="Welsh">Welsh</option><option value="Xhosa">Xhosa</option> </select></div><div class="form-group col-6"><label for="myRange">Proficiency with this language*</label><select class="form-control languageProficiency"name="ProficiencyLanguage[]" id="langRate"><option value="">Select One</option><option value="Beginner">Beginner</option><option value="Intermediate">Intermediate</option><option value="Expert">Expert</option></select></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });

    //degree

var educount = 0;
 $('.add-more-education').click(function(){
event.preventDefault();
educount++;
  $('.row-education').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">School Name</label><input type="text" class="form-control" name="schoolName[]"></div><div class="form-group col-6"><label for="End Date">School Location</label><input type="text" class="form-control" name="schoolLocation[]"></div><div class="form-group col-6"><label for="End Date">Degree</label><select class="form-control" data-live-search="true" name="degree[]"><option data-tokens="Ph.D">Ph.D</option><option data-tokens="Arts">Arts</option><option data-tokens="IT">IT</option></select></div><div class="form-group col-6"><label for="End Date">Field of Study</label><input type="text" class="form-control" name="studyField[]"></div><div class="form-group col-6"><label for="End Date">Graduation Start Date</label><input type="date" class="form-control" id="edu_start_date'+educount+'" name="graduationStart[]"></div><div class="form-group col-6"><label for="End Date">Graduation End Date</label><input type="date" class="form-control" id="edu_end_date'+educount+'" name="graduationEnd[]"><div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2"><input name="currently_attending[]" id="currentlyAttending'+educount+'" class="custom-control-input" type="checkbox" ><label for="currentlyAttending'+educount+'" class="custom-control-label">I currently attend here</label></div></div><div class="form-group col-12"><label for="End Date">Education summary</label><textarea  class="form-control"  name="education_summary[]" rows="5"></textarea></div><div class="align-self-end col-6 form-group ml-auto text-right""><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });


//job

var workcount = 0;
$('.add-more-job').click(function(){
            event.preventDefault();
            workcount++;
  $('.add-job').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="Job Title">Job Title</label><input type="text" class="form-control" name="jobTitle[]"></div><div class="form-group col-6"><label for="Area of Work">Area of Work</label><input type="text" class="form-control" name="employer[]"></div><div class="form-group col-6"><label for="City">City</label><input type="text" class="form-control" name="whCity[]"></div><div class="form-group col-6"><label for="State">State</label><input type="text" class="form-control" name="state[]"></div><div class="form-group col-6"><label for="Start Date">Start Date</label><input type="date" id="startDate'+workcount+'" class="form-control" name="startDate[]"></div><div class="form-group col-6"><label for="End Date">End Date</label><input type="date" class="form-control" id="endtDate'+workcount+'" name="endtDate[]"><div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2"><input name="currentlyWorking[]" id="currentlyWorking'+workcount+'" class="custom-control-input" type="checkbox" value="currently Working"><label for="currentlyWorking'+workcount+'" class="custom-control-label">I currently working here</label></div></div><div class="form-group col-12"><label for="End Date">Work Summary</label><textarea  class="form-control"  name="work_summary[]" rows="5"></textarea></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });

    //certificate
$('.add-more-certificate').click(function(){
  $('.certificate-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-3 row form-row"><div class="form-group col-4"><label for="End Date">Select Date</label><input type="date" class="form-control" id="certificDate" name="certificDate[]"></div><div class="form-group col-4"><label for="End Date">Certificate Name</label><input type="text" class="form-control" name="certificateName[]"></div><div class="form-group col-4"><label for="End Date">Certification Area</label><input type="text" class="form-control" name="certificateArea[]"></div><div class="form-group col-12"><label for="End Date">Certificate Summary</label><textarea  class="form-control"  name="certificate_summary[]" rows="5"></textarea></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });
    //interests
$('.add-more-interests').click(function(){
  $('.interests-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">interests</label><input type="text" class="form-control" name="interests[]"></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });
$(document).on('click', '.remove', function(){
  $(this).parent().parent('.row').remove();
  });


});

$('.customCheckbox input[type="checkbox"]').on('click', function () {
  if($(this).is(':checked')){
    $(this).parent().siblings('input[type="date"]').attr('disabled', 'disabled').val('').css('cursor', 'not-allowed');
  }
  else {
    $(this).parent().siblings('input[type="date"]').removeAttr('disabled', 'disabled').css('cursor', 'text');
  }
 });
$(document).on('click', '.customCheckbox input[type="checkbox"]', function () {
  if($(this).is(':checked')){
    $(this).parent().siblings('input[type="date"]').attr('disabled', 'disabled').val('').css('cursor', 'not-allowed');
  }
  else {
    $(this).parent().siblings('input[type="date"]').removeAttr('disabled', 'disabled').css('cursor', 'text');
  }
 });




$(document).ready(function () {

    $('fieldset:first-of-type').fadeIn('slow');
    $('#progressbar li:first-child').addClass('active');


    $('.sign-form .required-field').on('focus', function () {
        $(this).removeClass('border-danger');
    });

    // next step
    $('.sign-form .btn-next').on('click', function () {
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;
        parent_fieldset.find('.required-field').each(function () {
            if ($(this).val() == "") {
                $(this).addClass('border-danger');
                next_step = false;
            } else {
                $(this).removeClass('border-danger');
            }
        });
        $('.languageProficiency').on('change', function(){
          if ($(this).val() !== "") {
            $(this).removeClass('border-danger');
          }
        });
        $('.language-row').find('.chooseLanguage').each(function () {
            if ($(this).val() !== "") {
              if ($(this).parent().siblings('.form-group').children('.languageProficiency').val() == "") {
                $(this).parent().siblings('.form-group').children('.languageProficiency').addClass('border-danger');
                next_step = false;
            } else {
               $(this).parent().siblings('.form-group').children('.languageProficiency').removeClass('border-danger');
            }
          }
        });
        if (next_step) {
            parent_fieldset.fadeOut(400, function () {
            $(this).next().fadeIn();
            $('#progressbar li.active').next().addClass('active');
        });
        }
    });

    // previous step
    $('.sign-form .btn-previous').on('click', function () {
        $(this).parents('fieldset').fadeOut(400, function () {
            $(this).prev().fadeIn();
            $('#progressbar li.active:last').removeClass('active');
        });
    });

    // submit
    $('.sign-form').on('submit', function (e) {
        $(this).find('.required-field').each(function () {
            if ($(this).val() == "") {
                e.preventDefault();
                $(this).addClass('border-danger');
            } else {
                $(this).removeClass('border-danger');
            }
        });
    });
});
// To style all selects
$('.selectpicker').selectpicker();

//Show pop up when try to submit
// $(document).on('submit', '#msform',  function() {
//     var firstName = $('.firstName').val();
//     var lastName = $('.lastName').val();
//     var name = firstName + ' ' + lastName;
//     $('.confirm-dailogue-name').val(name);
//     $('.confirm-dailogue-email').val($('.email').val());
//     $('#registerModal').modal('show');
//     if($("#is_full_time").val() == ''){
//         return false;
//     }
// });

//on change radio button
var is_full_time = false;
$(document).on('change', 'input[name=optradio]', function(){
    if($(this).val() == 'one_time'){
        $('.basic-info').addClass('d-none');
        is_full_time = false;
    }else if($(this).val() == 'full_time'){
        $('.basic-info').removeClass('d-none');
        is_full_time = true;
    }
    // $(this).attr('id', 'noLastForm');
});

$('input[name=password').focusin(function(){
    $('#password-field').children().not(".toggle-password").remove();
    $('#password-field').removeClass('is-invalid');
});

var last_form = false;
$(document).on('click', '.save-btn', function(){
    if(is_full_time == true){
        if($('#password-field').val() != $('#confirm-password').val()){
            $("#password-field").after('<span class="invalid-feedback" role="alert"><strong>The password did not match</strong>');
            $("#password-field").addClass('is-invalid');
            return false;
        }
    }
    $(this).attr('disabled', true);
    $(this).val('Please wait...');
    $('#confirmation_name').val($('.confirm-dailogue-name').val());
    $('#confirmation_password').val($('#password-field').val());
    $("#confirmation_email").val($('.confirm-dailogue-email').val());
    var full_time = is_full_time == true ? 'yes' : 'no';
    $("#is_full_time").val(full_time);
    $('.submitLastForm').click();
});

//password show and hide
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
});

$(".toggle-confirm-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
});

//Next button click each time, this function is going to call
$(document).on('submit', '.basicInfoForm', function(e) {
    e.preventDefault();
    $(this).find("button[type='submit']").prop('disabled',true);
    $(this).find('button[type=submit]').text('Please wait...');
    var parent_fieldset = $(this).parents('fieldset');
    var next_step = true;

    //execute only for first form
    if($('input[name=first_form]').val() == 'yes'){
        parent_fieldset.find('.required-field').each(function () {
            if ($(this).val() == "") {
                $(this).addClass('border-danger');
                next_step = false;
            } else {
                $(this).removeClass('border-danger');
            }
        });
    }

    if(next_step == false){
        $(this).find('button[type=submit]').attr('disabled', false);
        $(this).find('button[type=submit]').html('Next');
        return false;
    }
    //This code is gonna execute only for last form
    if($(this).attr('id') == 'lastForm'){

        // if last form enable the submit button
        $(this).find("button[type='submit']").prop('disabled',false);
        $(this).find('button[type=submit]').text('Done');

        $('input[name=last_form]').val('yes');
        $.ajax({
            url:"{{ url('get-basic-info') }}",
            dataType : 'json',
            type: 'get',
            data: { cvform_id : $('input[name=cvform_id]').val() },
            success: function(data){

                $('.confirm-dailogue-name').val(data.name);
                $('.confirm-dailogue-email').val(data.email);
                $('#registerModal').modal('show');
            }
        });

        //unset last form so that the control not to come this block
        $(this).attr('id', 'noLastForm');
        return false;

    }
    // if(last_form == true){
    //     $('input[name=last_form]').val('yes');
    // }
    e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        url: "{{ url('add-basic-info') }}",
        dataType: 'json',
        type: 'post',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function(){
            $(this).find("button[type='submit']").prop('disabled',true);
            $(this).find('button[type=submit]').text('Please wait...');
        },
        success: function(result){
            $(this).find("button[type='submit']").prop('disabled',false);
            $(this).find('button[type=submit]').text('Next');
            if(result.success == true && result.last_form == true){
                $('.save-btn').attr('disabled', false);
                $('.save-btn').val('Submit');
                url = '{{ route("download.pdfAction", ":id") }}';
                url = url.replace(':id', result.cvform_id);
                Swal.fire({
                    title :'Good job!',
                    text : is_full_time == true ? 'Your Account has been created!, Your credential has been sent to you via email' : 'Your Resume is ready!',
                    icon:  'success',
                    confirmButtonText: 'Ok',
                }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
                //  else if (result.isDenied) {
                //     Swal.fire('Changes are not saved', '', 'info')
                // }
                })
            }else{
                parent_fieldset.fadeOut(400, function () {
                    $(this).next().fadeIn();
                    $(this).next().find('input[name=cvform_id]').val(result.cvform_id);
                    $('#progressbar li.active').next().addClass('active');
                });
            }
        }
      });

});
</script>
</body>
</html>

<style>
  .file-field input[type="file"] {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    padding: 0;
    margin: 0;
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 0;
}
.btn-mdb-color {
    color: #fff;
    font-size: 12px;
  }

</style>
