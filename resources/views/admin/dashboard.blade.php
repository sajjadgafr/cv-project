@extends('admin.layouts.app')

@section('content')


<div class="bg-white main-titl-row  pb-3 pt-3 row align-items-center ">
  <div class="col-lg-8 col-md-7 col-sm-7  title-col">
    <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Clinic dashboard</h3>
  </div>
  <div class="col-lg-4 col-md-5 col-sm-5 search-col">
     <div class="input-group custom-input-group">
          <input type="text" class="form-control" placeholder="Search">
          <div class="input-group-prepend">
            <span class="input-group-text fa fa-search"></span>
          </div>
     </div>
  </div>
</div>

<div class="row menulinkrow mb-3 mb-md-2 mb-xl-0">
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#6ab441">
            <i class="fa fa-user-md"></i> <span class="text-capitalize">Doctor</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#017baa">
            <i class="fa fa-wheelchair"></i> <span class="text-capitalize">Patient</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#812991">
            <i class="fa fa-clock-o"></i> <span class="text-capitalize">Appintment</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#d61f4b">
            <i class="fa fa-users"></i> <span class="text-capitalize">Users</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#812991">
            <i class="fa fa-users"></i> <span class="text-capitalize">messages</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#6ab441">
            <i class="fa fa-home"></i> <span class="text-capitalize">Procedure</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#d61f4b">
            <i class="fa fa-dollar"></i> <span class="text-capitalize">Payment</span>
    </div>
  </a>
  <a href="#" class="col-lg-3 col-md-4 col-6 menulink">
    <div class="d-flex flex-row-reverse align-items-center justify-content-between p-xl-4 pt-4 pb-4 pl-3 pr-3 text-white" style="background-color:#017baa">
             <i class="fa fa-cog"></i> <span class="text-capitalize">setting</span>
    </div>
  </a>
</div>



<div class="modal fade" id="viewmodal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>





<!-- Appointment Start Here -->
{{-- <div class="row">
  <div class="col-lg-12 col-12 appointment-col">
    <div class="bg-white custompadding customborder">
      <div class="section-header">
       <h5 class="mb-1">Appointment Chart</h5>
      </div>
      <div class="table-responsive">
        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-6 col-md-6 col-12 mb-3 mb-sm-0"><div class="dataTables_length text-sm-left" id="example_length"><label>Show <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6 col-md-6"><div id="example_filter" class="dataTables_filter text-sm-right"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="example" class="table table-bordered dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="example_info">
          <thead>
              <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Patient Name: activate to sort column descending" style="width: 130.5px;">Patient Name</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Patient Id: activate to sort column ascending" style="width: 214.5px;">Patient Id</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Phone Number: activate to sort column ascending" style="width: 131.5px;">Phone Number</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Appointment Id: activate to sort column ascending" style="width: 131.5px;">Appointment Id</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="date: activate to sort column ascending" style="width: 75.5px;">date</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Appointment Time: activate to sort column ascending" style="width: 158.5px;">Appointment Time</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 85.5px;">Action</th></tr>
          </thead>
          <tbody>
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
          <tr role="row" class="odd">
                  <td class="sorting_1">Airi Satou</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>33</td>
                  <td>2008/11/28</td>
                  <td>$162,700</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="even">
                  <td class="sorting_1">Airi Satou</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>33</td>
                  <td>2008/11/28</td>
                  <td>$162,700</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="odd">
                  <td class="sorting_1">Ashton Cox</td>
                  <td>Junior Technical Author</td>
                  <td>San Francisco</td>
                  <td>66</td>
                  <td>2009/01/12</td>
                  <td>$86,000</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="even">
                  <td class="sorting_1">Brielle Williamson</td>
                  <td>Integration Specialist</td>
                  <td>New York</td>
                  <td>61</td>
                  <td>2012/12/02</td>
                  <td>$372,000</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="odd">
                  <td class="sorting_1">Brielle Williamson</td>
                  <td>Integration Specialist</td>
                  <td>New York</td>
                  <td>61</td>
                  <td>2012/12/02</td>
                  <td>$372,000</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="even">
                  <td class="sorting_1">Cedric Kelly</td>
                  <td>Senior Javascript Developer</td>
                  <td>Edinburgh</td>
                  <td>22</td>
                  <td>2012/03/29</td>
                  <td>$433,060</td>
                  <td><div class="d-flex text-center">
                       <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="odd">
                  <td class="sorting_1">Cedric Kelly</td>
                  <td>Senior Javascript Developer</td>
                  <td>Edinburgh</td>
                  <td>22</td>
                  <td>2012/03/29</td>
                  <td>$433,060</td>
                  <td><div class="d-flex text-center">
                     <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="even">
                  <td class="sorting_1">Colleen Hurst</td>
                  <td>Javascript Developer</td>
                  <td>San Francisco</td>
                  <td>39</td>
                  <td>2009/09/15</td>
                  <td>$205,500</td>
                  <td><div class="d-flex text-center">
                    <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="odd">
                  <td class="sorting_1">Colleen Hurst</td>
                  <td>Javascript Developer</td>
                  <td>San Francisco</td>
                  <td>39</td>
                  <td>2009/09/15</td>
                  <td>$205,500</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr><tr role="row" class="even">
                  <td class="sorting_1">Garrett Winters</td>
                  <td>Accountant</td>
                  <td>Tokyo</td>
                  <td>63</td>
                  <td>2011/07/25</td>
                  <td>$170,750</td>
                  <td><div class="d-flex text-center">
                      <a data-toggle="modal" href="#viewmodal" class="actionicon bg-primary viewaction"><i class="fa fa-eye"></i></a>
                      <a data-toggle="modal" href="#editModal" class="actionicon bg-info editaction"><i class="fa fa-pencil"></i></a> 
                      <a data-toggle="modal" href="#deleteModal" class="actionicon bg-danger deleteaction"><i class="fa fa-close"></i></a>
                    </div></td>
              </tr></tbody>
      </table></div></div><div class="row"><div class="col-sm-5 col-md-5"><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 18 entries</div></div><div class="col-sm-7 col-md-7 col-12 mt-3 mt-sm-0"><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
    </div>
   </div>
  </div>

</div> --}}
@stop