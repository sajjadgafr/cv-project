@extends('admin.layouts.app')
@section('title', 'Page Title')
@section('content')

  <div class="main-titl-row pt-3 row align-items-center">
  <div class="col-lg-8 col-md-7 col-sm-7  title-col">
    <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">User Profile</h3>
  </div>
  <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
      <a href="#"  class="btn">Back</a>
  </div>

</div>

<div class="row user-pro-row">
	<div class="col-12 user-pro-col">
		<div class="customborder mx-0 py-3 row bg-white">
			<figure class="col-sm-2 mb-0">

	
				@if(!empty($cvform->avatar))
				  <img src="{{ asset( $cvform->avatar )}}" alt="user image" class="img-fluid img-thumbnail w-100">
				@else
				 <img src="{{ asset('/admin/assets/img/profile-placeholder.png') }}" alt="user image" class="img-fluid img-thumbnail w-100">
				@endif
				
			</figure>

			<div class="col-sm-5 pr-5 user-desc-col">
				<h4 class="text-capitalize">{{$cvform->firstName}} {{$cvform->lastName}}</h4>
				<p class="job-title text-capitalize mb-2"><em class="fa fa-briefcase"></em> {{$cvform->profession}}</p>
				<p class="company-name mb-2"><em class="fa fa-building"></em> Akits</p>
				<p class="company-name mb-2"><em class="fa fa-map-marker"></em> {{$cvform->streetAddress}}, {{$cvform->city}}, Pakistan</p>
			</div>
			<div class="border-left col-sm-5 pl-5 user-address-col">
				<table class="mb-0 table table-borderless">
					<tbody>
						
						<tr>
							<th class="text-nowrap">Phone No:</th>
							<td>{{$cvform->phone}}</td>
						</tr>
					
						<tr>
							<th class="text-nowrap">Email Address:</th>
							<td style="word-break: break-all;">{{$cvform->email}}</td>
						</tr>
						@if(!empty($cvform->date_of_birth))
						<tr>
							<th class="text-nowrap">Date of Birth:</th>
							<td>{{$cvform->date_of_birth}}</td>
						</tr>
						@endif
						@if(!empty($cvform->marital_status))
						<tr>
							<th class="text-nowrap">Marital Status:</th>
							<td>{{$cvform->marital_status}}</td>
						</tr>
						@endif
						@if(!empty($cvform->id_card))
						<tr>
							<th class="text-nowrap">ID card Number</th>
							<td>{{$cvform->id_card}}</td>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
		<div class="row mt-4">
			
			<div class="col-8 pro-center-col">


				<ul class="nav nav-tabs profiletabs" id="myTab" role="tablist">
				  <li class="nav-item">
				    <a class="nav-link active" data-toggle="tab" href="#profiletab1" role="tab" aria-controls="Summary" aria-selected="true">Summary</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#profiletab2" role="tab" aria-controls="Work History" aria-selected="false">Work History</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#profiletab3" role="tab" aria-controls="Education" aria-selected="false">Education</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#profiletab4" role="tab" aria-controls="Skills" aria-selected="false">Skills</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#profiletab5" role="tab" aria-controls="Additional information" aria-selected="false">Additional information</a>
				  </li>
				  <li class="nav-item">
				    <a class="nav-link" data-toggle="tab" href="#profiletab6" role="tab" aria-controls="Additional information" aria-selected="false">Certificate</a>
				  </li>
				</ul>

				<div class="tab-content customborder p-4  bg-white">
					@if(!empty($cvform->summary))
					  <div class="tab-pane fade show active prof-center-sect" id="profiletab1">
						<h4 class="font-weight-bold mb-3 text-capitalize">Summary</h4>
						<p>{{$cvform->summary}}</p>
					  </div>
					 @endif
					  <div class="tab-pane fade prof-center-sect" id="profiletab2">

						<h4 class="font-weight-bold mb-3 text-capitalize">Work history</h4>
						<ul class="list-unstyled profileSectList">
						  @if(!empty($workHistory))
							@foreach($workHistory as $work)
							<li>
								<h6 class="font-weight-bold">{{$work->jobTitle}}</h6>
							   @if(!empty($work))
								<div class="d-flex justify-content-between mb-3">
								<span class="history-company"><em class="fa fa-building mr-2"></em>{{$work->employer}}</span>
								<span class="history-start-end"><em class="fa fa-calendar mr-2"></em>{{$work->startDate}} - {{$work->endtDate}}</span>
								</div>
							  @endif
								<p>{{$work->work_summary}}</p>
							</li>
							@endforeach
						   @endif
						</ul>	
					  </div>
					  <div class="tab-pane fade prof-center-sect" id="profiletab3">
						<h4 class="font-weight-bold mb-3 text-capitalize">Education</h4>
						<ul class="list-unstyled profileSectList">
							@foreach($education as $edu)
							<li>
								<h6 class="font-weight-bold">{{$edu->studyField}}</h6>
								<div class="d-flex justify-content-between mb-3">
								<span class="edu-start-end mr-2"><em class="fa fa-graduation-cap mr-2"></em>{{$edu->degree}}</span>
								<span class="edu-area mr-2"><em class="fa fa-building mr-2"></em>{{$edu->schoolName}}</span>
								<span class="edu-location mr-2"><em class="fa fa-map-marker mr-2"></em>{{$edu->schoolLocation}}</span>
								<span class="history-start-end"><em class="fa fa-calendar mr-2"></em>{{$edu->graduationStart}} - {{$edu->graduationEnd}}</span>
								</div>
								<p>Sajjad has been truly great for us. Fast delivery, quick to post changes when needed and overall a great designer. Would definitely hire again!</p>
							</li>
							@endforeach
							
						</ul>	
					  </div>
					  <div class="tab-pane fade prof-center-sect" id="profiletab4">
						<h4 class="font-weight-bold mb-3 text-capitalize">Skill</h4>
						<ul class="list-unstyled skillslist mb-0">
					 @foreach($skill as $skills)
						<li class="list-inline-item mb-2"><span class="badge badge-pill badge-light border font-weight-normal px-3 py-2">{{$skills->skill}} <span class="align-middle badge badge-pill bg-info ml-1 p-0 text-white">{{$skills->skillProgress}}</span></span></li>
					 @endforeach
					
					</ul>
					  </div>
					  <div class="tab-pane fade prof-center-sect" id="profiletab5">
						<h4 class="font-weight-bold mb-3 text-capitalize">Additional information</h4>
						<p>{{$cvform->AdditionalNformation}}</p>
					  </div>
					  <div class="tab-pane fade prof-center-sect" id="profiletab6">
						<h4 class="font-weight-bold mb-3 text-capitalize">Certificate</h4>
						<ul class="list-unstyled profileSectList">
							@foreach($certificat as $certi)
							<li>
								<h6 class="font-weight-bold">{{$certi->certificateName}}</h6>
								<div class="d-flex justify-content-between mb-3">
								<span class="history-company"><em class="fa fa-building mr-2"></em>{{$certi->certificate_area}}</span>
								<span class="history-start-end"><em class="fa fa-calendar mr-2"></em>{{$certi->certificDate}}</span>
								</div>
								<p>{{$certi->certificate_summary}}</p>
							</li>
							@endforeach
						</ul>	
					  </div>
					
				</div>
			</div>
			<div class="col-4">
				<div class="customborder p-3 bg-white">
					<div class="pro-center-sect">
					<h4 class="font-weight-bold mb-3 text-capitalize">Social Links</h4>
					<ul class="list-unstyled sociallinks">
						 @foreach($social as $social_links)
						<li class="mb-3 d-flex align-items-center"><em class=" text-white rounded-circle text-center mr-2 fa fa-{{$social_links->sociallinkName}}"></em> <span>{{$social_links->socialLink}}</span></li>
						@endforeach
					</ul>
					</div>

					<div class="pro-center-sect mt-4 pt-4 border-top">
					<h4 class="font-weight-bold mb-3 text-capitalize">Languages</h4>
						<table class="table table-borderless table-striped">
							<tbody>
								@foreach($language as $lang)
								<tr>
									<td class="w-100">{{$lang->chooseLanguage}}</td>
									<td>{{$lang->ProficiencyLanguage}}</td>
								</tr>
								
								@endforeach
							</tbody>
						</table>
					</div> 
					<div class="pro-center-sect mt-4 pt-4 border-top">
					<h4 class="font-weight-bold mb-3 text-capitalize">interests</h4>
						<ul class="list-unstyled skillslist mb-0">
							@foreach($interest as $interests)
						<li class="list-inline-item mb-2"><span class="badge badge-pill badge-light border font-weight-normal px-3 py-2">{{$interests->interests}}</span></li>
						@endforeach
					</ul>
					</div>				 
				</div>
			</div>
		</div>
	</div>
</div>


@stop