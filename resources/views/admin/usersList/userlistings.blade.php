@extends('admin.layouts.app')

@section('content')
  <div class="main-titl-row pt-3 row align-items-center">
  <div class="col-lg-8 col-md-7 col-sm-7  title-col">
    <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Users List</h3>
  </div>
 {{--  <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
      <button type="button" class="btn" data-toggle="modal" href='#add_new_doctor'>Add new Doctor</button>
  </div> --}}

</div>
<div class="row">
  <div class="col-lg-12 col-12 appointment-col">
    <div class="bg-white custompadding customborder">
      <!-- <div class="section-header">
       <h5 class="mb-1">User List</h5>
      </div> -->
      <div class="table-responsive">
        <table id="example" class="table tabel-align-middle cvuserslist">
          <thead>
              <tr>

                  <th>Avatar</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Profession</th>
                   {{-- <th>city</th> --}}
                  {{-- <th>Street Address</th> --}}
                  {{-- <th>State/Province</th> --}}
                  <th>Phone</th>
                  <th>Email Address</th>
                  {{-- <th>Zip Code</th> --}}
                  {{-- <th>Summary</th> --}}
                  {{-- <th>Information</th> --}}
                  <th class="text-center">Action</th>
              </tr>
          </thead>
          <tbody>
          	@foreach($userList as $user)
              <tr>
                <td class="cvprofileimg">
                  <figure class="align-items-center bg-light border d-flex justify-content-center mb-0 overflow-hidden rounded-circle">
                @if(!empty($user->avatar))
                <img src="{{asset($user->avatar) }}" width="30px;" height="40px" class="img-fluid">
                @else
                  <img src="{{ asset('/admin/assets/img/profile-placeholder.png') }}" alt="user image" class="img-fluid">
                @endif
                </figure>
                </td>
                  <td>{{ $user->firstName}}</td>
                  <td>{{ $user->lastName}}</td>
                  <td>{{ $user->profession}}</td>
                  {{-- <td>{{ $user->city}}</td>
                  <td>{{ $user->streetAddress}}</td>
                  <td>{{ $user->stateProvince}}</td> --}}
                  <td>{{ $user->phone}}</td>
                  <td>{{ $user->email}}</td>
                 {{--  <td>{{ $user->zipCode}}</td>
                  <td>{{substr($user->summary, 0,  15)}}</td>
                  <td>{{substr($user->AdditionalNformation, 0, 10)}}</td> --}}
                  <td><div class="d-flex justify-content-center text-center">
                    <a href="{{ route('cv.cv_pdf', ['id' => $user->id]) }}" title="Dwonload Pdf" class="actionicon border border-success editaction fa fa-download text-success"></a>
                     <a href="{{ route('users.cvprofile', ['id' => $user->id]) }}" title="Preview Profile" class="actionicon border border-success editaction fa fa-eye text-success"></a>
                      <a href="{{ route('users.edit', ['id' => $user->id]) }}"  title="Edit Profile"  class="actionicon border border-info editaction fa fa-pencil text-info"></a>

                      <form method="POST" action="{{ route('users.destroy', ['id' => $user->id] )}}">
                          @method('DELETE')
                         @csrf
                    <button type="submit" name="submit" onclick="return confirm('Are you sure you want to delete this item?'); "
                      class="actionicon bg-white border border-danger deleteaction fa fa-trash text-danger"></button>
                    </form>
                    </div></td>
              </tr>

            @endforeach


          </tbody>
      </table>
    </div>
   </div>
  </div>

</div>

@stop
