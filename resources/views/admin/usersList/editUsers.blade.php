@extends('admin.layouts.app')
@include('includes.head')
@section('content')
@php
    $work_count = 0;
    $educt_count = 0;
@endphp
<section class="form-wizard-section py-5">
<div class="container my-3">
  <div class="row">
    <div class="col-12">
      <ul id="progressbar" class="d-flex justify-content-between mb-5">
          <li class="active">Basic Info</li>
          <li>Work History</li>
          <li>Education</li>
          <li>Languages</li>
          <li>Skills</li>
          <li>Summary</li>
        </ul>
    </div>


  <div class="col-12">
    {{-- <form action="{{ route('users.update', $usersEdit->id) }}" method="post" enctype="multipart/form-data" id="msform" class="sign-form"> --}}
        {{-- {{ csrf_field() }} --}}

    <!-- Contact History Fields -->
    <fieldset id="fieldset-1">
        <form action="" class="basicInfoForm">
            @csrf
            <input type="hidden" name="cvform_id" value="">
            <input type="hidden" name="first_form" value="yes">
            <input type="hidden" name="edit_form" id="editForm" value="yes">
           <div class="form-title-col mb-4">
            <h3 class="form-title">What’s the best way for employers to contact you?</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>
      <div class="basic-info-row">
        <div class="row">
        <div class="form-group col-6">
          <label for="firstName">First Name <sup>*</sup></label>
          <input type="text" class="form-control required-field" name="firstName" value="{{$usersEdit->firstName}}">
        </div>
        <div class="form-group col-6">
          <label for="lastName">Last Name <sup>*</sup></label>
          <input type="text" class="form-control required-field" name="lastName" value="{{$usersEdit->lastName}}">
        </div>
        <div class="form-group col-6">
          <label for="Profession">Profession</label>
          <input type="text" class="form-control" name="profession" value="{{$usersEdit->profession}}">
        </div>
        <div class="form-group col-6">
          <label for="Street Address">Street Address</label>
          <input type="text" class="form-control" name="streetAddress" value="{{$usersEdit->streetAddress}}">
        </div>
        <div class="form-group col-6">
          <label for="City">City</label>
          <input type="text" class="form-control" name="city" value="{{$usersEdit->city}}">
        </div>
        <div class="form-group col-6">
          <label for="Country/State">Country/State</label>
          <input type="text" class="form-control" name="stateProvince" value="{{$usersEdit->stateProvince}}">
        </div>
        <div class="form-group col-6">
          <label for="Zip Code">Zip Code</label>
          <input type="tel" class="form-control" name="zipCode" value="{{$usersEdit->zipCode}}">
        </div>
        <div class="form-group col-6">
          <label for="Date of Birth">Date of Birth</label>
          <input type="date" class="form-control" name="dateOfBirth" value="{{$usersEdit->date_of_birth}}">
        </div>
        <div class="form-group col-6">
          <label for="Marital Status">Marital Status</label>
          <select class="form-control" name="maritalStatus" value="{{$usersEdit->marital_status}}">
            <option value="Single" {{ $usersEdit->marital_status == 'Single' ? 'selected' : '' }}>Single</option>
            <option value="Married" {{ $usersEdit->marital_status == 'Married' ? 'selected' : '' }}>Married</option>
            <option value="Separated" {{ $usersEdit->marital_status == 'Separated' ? 'selected' : '' }}>Separated</option>
            <option value="Divorced" {{ $usersEdit->marital_status == 'Divorced' ? 'selected' : '' }}>Divorced</option>
            <option value="Widowed" {{ $usersEdit->marital_status == 'Widowed' ? 'selected' : '' }}>Divorced</option>
          </select>
        </div>
        <div class="form-group col-6">
          <label for="ID card Number">ID card Number (Optional)</label>
          <input type="tel" class="form-control" name="idCard" value="{{$usersEdit->id_card}}">
        </div>
        <div class="form-group col-6">
          <label for="Phone">Phone <sup>*</sup></label>
          <input type="tel" class="form-control required-field" name="phone" value="{{$usersEdit->phone}}">
        </div>
         <div class="form-group col-6">
          <label for="Email Address">Email Address <sup>*</sup></label>
          <input type="email" class="form-control required-field" name="email" value="{{$usersEdit->email}}">
        </div>
        <div class="form-group col-6">
          <label for="Avatar">Avatar</label><br />
          <img src="{{ asset( '/'.$usersEdit->avatar )}}" alt="user image" width="100px" height="50px" class="pb-1">
          <input type="file" class="form-control" name="avatar" value="{{$usersEdit->avatar}}">
        </div>
           </div>
          </div>
           <div class="form-group">
               @foreach($socialLinkEdit as $socialEd)
               <div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row">
                   <div class="form-group col-6">
                       <label for="End Date">Social Website</label>

                       <select class="form-control" name="sociallinkName[]" value="{{ $socialEd['sociallinkName']  }}">
                           <option value="facebook" {{ $socialEd['sociallinkName'] == 'facebook' ? 'selected' : '' }}>facebook</option>
                           <option value="twitter" {{ $socialEd['sociallinkName'] == 'twitter' ? 'selected' : '' }}>twitter</option>
                           <option value="linkedin" {{ $socialEd['sociallinkName'] == 'linkedin' ? 'selected' : '' }}>linkedin</option>
                       </select>
                   </div>
                   <div class="form-group col-6">
                       <label for="myRange">Social Link</label>
                       <input type="text" class="form-control" name="socialLink[]" value="{{$socialEd['socialLink']}}">
                   </div>
                   <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
                </div>
               @endforeach
                <span class="fa fa-plus-square mr-2 text-primary add-social-link">Add Social Link</span>
           </div>
            <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
              <button type="submit" class="btn btn-next ml-auto">Next</button>
            </div>
        </form>
        </fieldset>

        <!-- Work History Fields -->

        <fieldset id="fieldset-2">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Now, let’s fill out your work history</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>


          <div class="add-job">
              @foreach($workHistory as $workEdit)
            <?php //echo $workEdit['$workEdit']
                $work_count++;
            ?>
          <div class="row">
          <div class="form-group col-6">
          <label for="Job Title">Job Title</label>
          <input type="text" class="form-control" name="jobTitle[]" value="{{$workEdit['jobTitle']}}">
         </div>

        <div class="form-group col-6">
          <label for="Area of Work">Area of Work</label>
          <input type="text" class="form-control" name="employer[]" value="{{$workEdit['employer']}}">
        </div>

        <div class="form-group col-6">
          <label for="City">City</label>
          <input type="text" class="form-control" name="whCity[]" value="{{$workEdit['whCity']}}">
        </div>

        <div class="form-group col-6">
          <label for="State">State</label>
          <input type="text" class="form-control" name="state[]" value="{{$workEdit['state']}}">
        </div>
        <div class="form-group col-6">
          <label for="Start Date">Start Date</label>
          <input type="date" class="form-control" id="startDate" name="startDate[]" value="{{$workEdit['startDate']}}">
        </div>
        <div class="form-group col-6">
          <label for="End Date">End Date</label>
          <input type="date" class="form-control" id="endtDate" name="endtDate[]" value="{{$workEdit['endtDate']}}">
          <div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2">
            @if($workEdit['currently_working'] == null || $workEdit['currently_working'] == 0)
                <input type="hidden" name="currentlyWorking[]" value="0">
            @endif
          <input type="checkbox" data-type="work" name="currentlyWorking[]" id="currentlyWorking{{$work_count}}" class="custom-control-input" value="1" {{$workEdit['currently_working'] == 1 ? 'checked':''}}>
          <label for="currentlyWorking{{$work_count}}"  class="custom-control-label">I currently working here</label></div>
        </div>
        <div class="form-group col-12">
          <label for="End Date">Work Summary</label>
          <textarea  class="form-control workHistory"  name="work_summary[]" rows="5">{{$workEdit['work_summary']}}</textarea>
        </div>
        <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
       </div>
              @endforeach

        </div>
              <div class="text-primary form-group add-more-job"><em class="fa fa-plus-square mr-2"></em> Add More</div>

                <br><hr><br>

              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>

        <!-- Education Fields Start here -->


        <fieldset id="fieldset-3">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Great, let’s work on your education</h3>
            <!-- <p class="form-sub-title">We suggest including an email and phone number.</p> -->
           </div>

             <div class="row-education">
         @foreach($education as $eduEdit)
         @php
             $educt_count++;
         @endphp
           <div class="row">
            <div class="form-group col-6">
            <label for="End Date">School Name</label>
            <input type="text" class="form-control" name="schoolName[]" value="{{$eduEdit['schoolName']}}">
          </div>
          <div class="form-group col-6">
            <label for="End Date">School Location</label>
            <input type="text" class="form-control" name="schoolLocation[]" value="{{$eduEdit['schoolLocation']}}">
          </div>
          <div class="form-group col-6">
            <label for="End Date">Degree</label>
          <select class="form-control" data-live-search="true" name="degree[]" value="{{$eduEdit['degree']}}">
              <option value="Ph.D" {{ $eduEdit['degree'] == 'Ph.D' ? 'selected' : '' }}>Ph.D</option>
              <option value="Arts" {{ $eduEdit['degree'] == 'Arts' ? 'selected' : '' }}>Arts</option>
              <option value="IT" {{ $eduEdit['degree'] == 'IT' ? 'selected' : '' }}>IT</option>
          </select>

          </div>
          <div class="form-group col-6">
            <label for="End Date">Field of Study</label>
            <input type="text" class="form-control" name="studyField[]" value="{{$eduEdit['studyField']}}">
          </div>
          <div class="form-group col-6">
            <label for="End Date">Graduation Start Date</label>
            <input type="date" class="form-control" id="edu_start_date" name="graduationStart[]" value="{{$eduEdit['graduationStart']}}">
          </div>
          <div class="form-group col-6">
            <label for="End Date">Graduation End Date</label>
              <input type="date" class="form-control" id="edu_end_date" name="graduationEnd[]" value="{{$eduEdit['graduationEnd']}}">

            <div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2">
            @if($eduEdit['currently_attending'] == null || $eduEdit['currently_attending'] == 0)
                <input type="hidden" name="currently_attending[]" value="0">
            @endif
            <input type="checkbox" data-type="education" name="currently_attending[]" id="currentlyAttending{{ $educt_count }}" class="custom-control-input"  value="1" {{$eduEdit['currently_attending'] == 1 ? 'checked': ''}} >
                <label for="currentlyAttending{{ $educt_count }}" class="custom-control-label">I currently attend here</label></div>
          </div>
          <div class="form-group col-12">
            <label for="End Date">Education Summary</label>
            <textarea  class="form-control educationSummerNote"  name="education_summary[]" rows="5">{{$eduEdit['education_summary']}}</textarea>
          </div>
          <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
           </div>
        @endforeach
             </div>
             <div class="text-primary form-group add-more-education"><em class="fa fa-plus-square mr-2"></em> Add More</div>
                <br><hr><br>

              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>

        <!-- // -->

        <!-- Languages Fields Start here -->
        <fieldset id="fieldset-4">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Add Language</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>

            <div class="language-row">
        @foreach($languageEdit as $langEdit)
           <div class="row">
            <div class="form-group col-6">
            <label for="End Date">Choose Language</label>

            <select data-placeholder="Choose a Language..." class="form-control " name="chooseLanguage[]" id="langName" value="{{$langEdit['chooseLanguage']}}">
                <option value="Afrikaans" {{$langEdit['chooseLanguage'] =='Afrikaans'? 'selected':''}}>Afrikaans</option>
                <option value="Albanian" {{$langEdit['chooseLanguage'] =='Albanian'? 'selected':''}}>Albanian</option>
                <option value="Arabic" {{$langEdit['chooseLanguage'] =='Arabic'? 'selected':''}}>Arabic</option>
                <option value="Armenian" {{$langEdit['chooseLanguage'] =='Armenian'? 'selected':''}}>Armenian</option>
                <option value="Basque" {{$langEdit['chooseLanguage'] =='Basque'? 'selected':''}}>Basque</option>
                <option value="Bengali" {{$langEdit['chooseLanguage'] =='Bengali'? 'selected':''}}>Bengali</option>
                <option value="Bulgarian" {{$langEdit['chooseLanguage'] =='Bulgarian'? 'selected':''}}>Bulgarian</option>
                <option value="Catalan" {{$langEdit['chooseLanguage'] =='Catalan'? 'selected':''}}>Catalan</option>
                <option value="Cambodian" {{$langEdit['chooseLanguage'] =='Cambodian'? 'selected':''}}>Cambodian</option>
                <option value="Chinese (Mandarin)" {{$langEdit['chooseLanguage'] =='Chinese (Mandarin)'? 'selected':''}}>Chinese (Mandarin)</option>
                <option value="Croatian" {{$langEdit['chooseLanguage'] =='Croatian'? 'selected':''}}>Croatian</option>
                <option value="Czech" {{$langEdit['chooseLanguage'] =='Czech'? 'selected':''}}>Czech</option>
                <option value="Danish" {{$langEdit['chooseLanguage'] =='Danish'? 'selected':''}}>Danish</option>
                <option value="Dutch" {{$langEdit['chooseLanguage'] =='Dutch'? 'selected':''}}>Dutch</option>
                <option value="English" {{$langEdit['chooseLanguage'] =='English'? 'selected':''}}>English</option>
                <option value="Estonian" {{$langEdit['chooseLanguage'] =='Estonian'? 'selected':''}}>Estonian</option>
                <option value="Fiji" {{$langEdit['chooseLanguage'] =='Fiji'? 'selected':''}}>Fiji</option>
                <option value="Finnish" {{$langEdit['chooseLanguage'] =='Finnish'? 'selected':''}}>Finnish</option>
                <option value="French" {{$langEdit['chooseLanguage'] =='French'? 'selected':''}}>French</option>
                <option value="Georgian" {{$langEdit['chooseLanguage'] =='Georgian'? 'selected':''}}>Georgian</option>
                <option value="German" {{$langEdit['chooseLanguage'] =='German'? 'selected':''}}>German</option>
                <option value="Greek" {{$langEdit['chooseLanguage'] =='Greek'? 'selected':''}}>Greek</option>
                <option value="Gujarati" {{$langEdit['chooseLanguage'] =='Gujarati'? 'selected':''}}>Gujarati</option>
                <option value="Hebrew" {{$langEdit['chooseLanguage'] =='Hebrew'? 'selected':''}}>Hebrew</option>
                <option value="Hindi" {{$langEdit['chooseLanguage'] =='Hindi'? 'selected':''}}>Hindi</option>
                <option value="Hungarian" {{$langEdit['chooseLanguage'] =='Hungarian'? 'selected':''}}>Hungarian</option>
                <option value="Icelandic" {{$langEdit['chooseLanguage'] =='Icelandic'? 'selected':''}}>Icelandic</option>
                <option value="Indonesian" {{$langEdit['chooseLanguage'] =='Indonesian'? 'selected':''}}>Indonesian</option>
                <option value="Irish" {{$langEdit['chooseLanguage'] =='Irish'? 'selected':''}}>Irish</option>
                <option value="Italian" {{$langEdit['chooseLanguage'] =='Italian'? 'selected':''}}>Italian</option>
                <option value="Japanese" {{$langEdit['chooseLanguage'] =='Japanese'? 'selected':''}}>Japanese</option>
                <option value="Korean" {{$langEdit['chooseLanguage'] =='Korean'? 'selected':''}}>Korean</option>
                <option value="Latin" {{$langEdit['chooseLanguage'] =='Latin'? 'selected':''}}>Latin</option>
                <option value="Latvian" {{$langEdit['chooseLanguage'] =='Latvian'? 'selected':''}}>Latvian</option>
                <option value="Lithuanian" {{$langEdit['chooseLanguage'] =='Lithuanian'? 'selected':''}}>Lithuanian</option>
                <option value="Macedonian" {{$langEdit['chooseLanguage'] =='Macedonian'? 'selected':''}}>Macedonian</option>
                <option value="Malay" {{$langEdit['chooseLanguage'] =='Malay'? 'selected':''}}>Malay</option>
                <option value="Malayalam" {{$langEdit['chooseLanguage'] =='Malayalam'? 'selected':''}}>Malayalam</option>
                <option value="Maltese" {{$langEdit['chooseLanguage'] =='Maltese'? 'selected':''}}>Maltese</option>
                <option value="Maori" {{$langEdit['chooseLanguage'] =='Maori'? 'selected':''}}>Maori</option>
                <option value="Marathi" {{$langEdit['chooseLanguage'] =='Marathi'? 'selected':''}}>Marathi</option>
                <option value="Mongolian" {{$langEdit['chooseLanguage'] =='Mongolian'? 'selected':''}}>Mongolian</option>
                <option value="Nepali" {{$langEdit['chooseLanguage'] =='Nepali'? 'selected':''}}>Nepali</option>
                <option value="Norwegian" {{$langEdit['chooseLanguage'] =='Norwegian'? 'selected':''}}>Norwegian</option>
                <option value="Persian" {{$langEdit['chooseLanguage'] =='Persian'? 'selected':''}}>Persian</option>
                <option value="Polish" {{$langEdit['chooseLanguage'] =='Polish'? 'selected':''}}>Polish</option>
                <option value="Portuguese" {{$langEdit['chooseLanguage'] =='Portuguese'? 'selected':''}}>Portuguese</option>
                <option value="Punjabi" {{$langEdit['chooseLanguage'] =='Punjabi'? 'selected':''}}>Punjabi</option>
                <option value="Quechua" {{$langEdit['chooseLanguage'] =='Quechua'? 'selected':''}}>Quechua</option>
                <option value="Romanian" {{$langEdit['chooseLanguage'] =='Romanian'? 'selected':''}}>Romanian</option>
                <option value="Russian" {{$langEdit['chooseLanguage'] =='Russian'? 'selected':''}}>Russian</option>
                <option value="Samoan" {{$langEdit['chooseLanguage'] =='Samoan'? 'selected':''}}>Samoan</option>
                <option value="Serbian" {{$langEdit['chooseLanguage'] =='Serbian'? 'selected':''}}>Serbian</option>
                <option value="Slovak" {{$langEdit['chooseLanguage'] =='Slovak'? 'selected':''}}>Slovak</option>
                <option value="Slovenian" {{$langEdit['chooseLanguage'] =='Slovenian'? 'selected':''}}>Slovenian</option>
                <option value="Spanish" {{$langEdit['chooseLanguage'] =='Spanish'? 'selected':''}}>Spanish</option>
                <option value="Swahili" {{$langEdit['chooseLanguage'] =='Swahili'? 'selected':''}}>Swahili</option>
                <option value="Swedish " {{$langEdit['chooseLanguage'] =='Swedish'? 'selected':''}}>Swedish</option>
                <option value="Tamil" {{$langEdit['chooseLanguage'] =='Tamil'? 'selected':''}}>Tamil</option>
                <option value="Tatar" {{$langEdit['chooseLanguage'] =='Tatar'? 'selected':''}}>Tatar</option>
                <option value="Telugu" {{$langEdit['chooseLanguage'] =='Telugu'? 'selected':''}}>Telugu</option>
                <option value="Thai" {{$langEdit['chooseLanguage'] =='Thai'? 'selected':''}}>Thai</option>
                <option value="Tibetan" {{$langEdit['chooseLanguage'] =='Tibetan'? 'selected':''}}>Tibetan</option>
                <option value="Tonga" {{$langEdit['chooseLanguage'] =='Tonga'? 'selected':''}}>Tonga</option>
                <option value="Turkish" {{$langEdit['chooseLanguage'] =='Turkish'? 'selected':''}}>Turkish</option>
                <option value="Ukrainian" {{$langEdit['chooseLanguage'] =='Ukrainian'? 'selected':''}}>Ukrainian</option>
                <option value="Urdu" {{$langEdit['chooseLanguage'] =='Urdu'? 'selected':''}}>Urdu</option>
                <option value="Uzbek" {{$langEdit['chooseLanguage'] =='Uzbek'? 'selected':''}}>Uzbek</option>
                <option value="Vietnamese" {{$langEdit['chooseLanguage'] =='Vietnamese'? 'selected':''}}>Vietnamese</option>
                <option value="Welsh" {{$langEdit['chooseLanguage'] =='Welsh'? 'selected':''}}>Welsh</option>
                <option value="Xhosa" {{$langEdit['chooseLanguage'] =='Xhosa'? 'selected':''}}>Xhosa</option>
            </select>


          </div>
          <div class="form-group col-6">
            <label for="myRange">Proficiency with this language*</label>
          <select class="form-control" name="ProficiencyLanguage[]" id="langRate" value="{{$langEdit['ProficiencyLanguage']}}">
            <option value="" >Select One</option>

            <option value="Beginner" {{$langEdit['ProficiencyLanguage'] == 'Beginner'?'selected':'' }}>Beginner</option>
            <option value="Intermediate" {{$langEdit['ProficiencyLanguage'] == 'Intermediate'?'selected':'' }}>Intermediate</option>
            <option value="Expert" {{$langEdit['ProficiencyLanguage'] == 'Expert'?'selected':'' }}>Expert</option>
            </select>
          </div>
          <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
           </div>
        @endforeach
          </div>
           <div class="text-primary form-group add-more-languages"><em class="fa fa-plus-square mr-2"></em> Add More Skills</div>
                <br><hr><br>

              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <!-- Skills Fields Start here -->
        <fieldset id="fieldset-5">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
                <div class="form-title-col mb-4">
            <h3 class="form-title">Add Skills</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>

            <div class="skills-row">
        @foreach($skillEdit as $skill)
           <div class="row">
            <div class="form-group col-6">
            <label for="End Date">Skill</label>
            <input type="text" class="form-control" name="skill[]" value="{{$skill['skill']}}">
          </div>
          <div class="form-group col-6">
            <label for="myRange">Skill Progress</label>
          <div class="slidecontainer d-flex justify-content-between align-items-center">
              <input type="range" min="0" max="100" name="skillProgress[]" value="{{$skill['skillProgress']}}" class="custom-range myRange" id="myRange">
              <div class="ml-3 text-nowrap">Value: <span class="d-inline-block font-weight-bold mr-1 myRangeVal">{{$skill['skillProgress']}}</span><em class="fa fa-percent"></em></div>
          </div>
          </div>
          <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
           </div>
        @endforeach
          </div>
           <div class="text-primary form-group add-more-skills"><em class="fa fa-plus-square mr-2"></em> Add More Skills</div>

              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <!-- Summary Fields Start here -->
        <fieldset id="fieldset-6">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Briefly tell us about your background</h3>
            <p class="form-sub-title">We suggest including an email and phone number.</p>
           </div>
            <div class="row">
              <div class="form-group col-12">
            <label for="End Date">Summary</label>
            <textarea class="form-control summarySummerNote" name="summary" rows="5">{{$usersEdit->summary}}</textarea>
          </div>
            </div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
        <!-- // -->

        <fieldset id="fieldset-8">
            <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">Additional information</h3>
            <p class="form-sub-title">Add anything else you want employers to know.</p>
           </div>
            <div class="row">
              <div class="form-group col-12">
            <label for="End Date">Additional information</label>
            <textarea class="form-control additionalInfoSummerNote" name="AdditionalNformation" rows="5">{{$usersEdit->AdditionalNformation}}</textarea>
          </div>
            </div>
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>

         <fieldset id="fieldset-9">
             <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">What certifications do you have?</h3>
            <p class="form-sub-title">Add anything else you want employers to know.</p>
           </div>

           <div class="certificate-row">
       @foreach($certificatEdit as $certificate)
            <div class="row form-row">
              <div class="form-group col-4">
            <label for="End Date">Select Date</label>
            <input type="date" class="form-control" id="certificDate" name="certificDate[]" value="{{$certificate['certificDate']}}">
          </div>
          <div class="form-group col-4">
            <label for="End Date">Certificate Name</label>
            <input type="text" class="form-control" name="certificateName[]" value="{{$certificate['certificateName']}}">
          </div>
          <div class="form-group col-4">
            <label for="End Date">Certification Area</label>
            <input type="text" class="form-control" name="certificateArea[]" value="{{$certificate['certificate_area']}}">
          </div>
          <div class="form-group col-12"><label for="End Date">Certificate Summary</label><textarea  class="form-control certificateSummerNote"  name="certificate_summary[]" rows="5">{{$certificate['certificate_summary']}}</textarea></div>
          <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
            </div>
       @endforeach
          </div>
          <div class="text-primary form-group add-more-certificate"><em class="fa fa-plus-square mr-2"></em> Add More</div>

              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn btn-next">Next</button>
              </div>
            </form>
        </fieldset>
         <fieldset>
             <form action="" class="basicInfoForm">
                @csrf
                <input type="hidden" name="cvform_id" value="">
                <input type="hidden" name="edit_form" value="yes">
          <div class="form-title-col mb-4">
            <h3 class="form-title">What interests do you have?</h3>
            <p class="form-sub-title">Add anything else you want employers to know.</p>
           </div>



           <div class="interests-row">
       @foreach($interestEdit as $interest)
            <div class="row">
          <div class="form-group col-6">
            <label for="End Date">interests</label>
            <input type="text" class="form-control" name="interests[]" value="{{$interest['interests']}}">
          </div>
          <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div>
            </div>
       @endforeach
          </div>
          <div class="text-primary form-group add-more-interests"><em class="fa fa-plus-square mr-2"></em> Add More</div>
          <input type="hidden" name="last_form" value="yes">
              <div class="d-flex form-btn-group form-group justify-content-between mb-0 mt-3">
                <button type="button" class="btn btn-previous">Previous</button>
                <button type="submit" class="btn submitbtn">Done</button>
              </div>
            </form>
        </fieldset>
  {{-- </form> --}}
   </div>
</div>
  </div>



{{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>
 --><script src="{{ asset('/assets/js/bootstrap-datepicker.min.js') }}" ></script> --}}


<script type="text/javascript">

//$('#startDate'+datecount1111).datepicker({});


// var slider = $('.myRange').val();
// var output = $('.myRangeVal').text(slider);

$(document).on('change', '.myRange', function(){
  $(this).siblings('div').children('.myRangeVal').html($(this).val());
});

$(document).ready(function () {

 //work history summernote initate
 $('.workHistory').summernote({
      height: 200,
    //   placeholder: 'Description (Required)',
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        // ['height', ['height']]
    ]
});

//education summernote initiate
 $('.educationSummerNote').summernote({
      height: 200,
    //   placeholder: 'Description (Required)',
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        // ['height', ['height']]
    ]
});

//summary summernote initiate
 $('.summarySummerNote').summernote({
      height: 200,
    //   placeholder: 'Description (Required)',
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        // ['height', ['height']]
    ]
});

//additionalInfo summernote initiate
 $('.additionalInfoSummerNote').summernote({
      height: 200,
    //   placeholder: 'Description (Required)',
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        // ['height', ['height']]
    ]
});

//certificate summernote initiate
 $('.certificateSummerNote').summernote({
      height: 200,
    //   placeholder: 'Description (Required)',
      toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        // ['height', ['height']]
    ]
});

$('.add-social-link').click(function(){
  $('.basic-info-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">Social Website</label><select class="form-control" name="sociallinkName[]" value""><option value="facebook" >facebook</option><option value="twitter" >twitter</option><option value="linkedin">linkedin</option> </select></div><div class="form-group col-6"><label for="myRange">Social Link</label><input type="text" class="form-control" name="socialLink[]" value=""></div> <div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');});

$('.add-more-skills').click(function(){
  $('.skills-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">Skill</label><input type="text" class="form-control" name="skill[]"></div><div class="form-group col-6"><label for="myRange">Skill Progress</label><div class="slidecontainer d-flex justify-content-between align-items-center"><input type="range" min="0" max="100" value="50" name="skillProgress[]" class="custom-range myRange" id="myRange"> <div class="ml-3 text-nowrap">Value: <span id="demo" class="d-inline-block font-weight-bold mr-1 myRangeVal">50</span><em class="fa fa-percent"></em></div></div></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });
$('.add-more-languages').click(function(){
  $('.language-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">Choose Language</label><select data-placeholder="Choose a Language..." class="form-control " name="chooseLanguage[]" id="langName"><option value="Afrikaans">Afrikaans</option><option value="Albanian">Albanian</option><option value="Arabic">Arabic</option><option value="Armenian">Armenian</option><option value="Basque">Basque</option><option value="Bengali">Bengali</option><option value="Bulgarian">Bulgarian</option><option value="Catalan">Catalan</option><option value="Cambodian">Cambodian</option><option value="Chinese (Mandarin)">Chinese (Mandarin)</option><option value="Croatian">Croatian</option><option value="Czech">Czech</option><option value="Danish">Danish</option><option value="Dutch">Dutch</option><option value="English">English</option><option value="Estonian">Estonian</option><option value="Fiji">Fiji</option><option value="Finnish">Finnish</option><option value="French">French</option><option value="Georgian">Georgian</option><option value="German">German</option><option value="Greek">Greek</option><option value="Gujarati">Gujarati</option><option value="Hebrew">Hebrew</option><option value="Hindi">Hindi</option><option value="Hungarian">Hungarian</option><option value="Icelandic">Icelandic</option><option value="Indonesian">Indonesian</option><option value="Irish">Irish</option><option value="Italian">Italian</option><option value="Japanese">Japanese</option><option value="Javanese">Javanese</option><option value="Korean">Korean</option><option value="Latin">Latin</option><option value="Latvian">Latvian</option><option value="Lithuanian">Lithuanian</option><option value="Macedonian">Macedonian</option><option value="Malay">Malay</option><option value="Malayalam">Malayalam</option><option value="Maltese">Maltese</option><option value="Maori">Maori</option><option value="Marathi">Marathi</option><option value="Mongolian">Mongolian</option><option value="Nepali">Nepali</option><option value="Norwegian">Norwegian</option><option value="Persian">Persian</option><option value="Polish">Polish</option><option value="Portuguese">Portuguese</option><option value="Punjabi">Punjabi</option><option value="Quechua">Quechua</option><option value="Romanian">Romanian</option><option value="Russian">Russian</option><option value="Samoan">Samoan</option><option value="Serbian">Serbian</option><option value="Slovak">Slovak</option><option value="Slovenian">Slovenian</option><option value="Spanish">Spanish</option><option value="Swahili">Swahili</option><option value="Swedish ">Swedish</option><option value="Tamil">Tamil</option><option value="Tatar">Tatar</option><option value="Telugu">Telugu</option><option value="Thai">Thai</option><option value="Tibetan">Tibetan</option><option value="Tonga">Tonga</option><option value="Turkish">Turkish</option><option value="Ukrainian">Ukrainian</option><option value="Urdu">Urdu</option><option value="Uzbek">Uzbek</option><option value="Vietnamese">Vietnamese</option><option value="Welsh">Welsh</option><option value="Xhosa">Xhosa</option> </select></div><div class="form-group col-6"><label for="myRange">Proficiency with this language*</label><select class="form-control"name="ProficiencyLanguage[]" id="langRate"><option value="">Select One</option><option value="1">Beginner</option><option value="2">Intermediate</option><option value="3">Expert</option></select></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });

    //degree

var educount = 0;
ed_ct = "{{ $educt_count }}";
educount += ed_ct;
 $('.add-more-education').click(function(e){
e.preventDefault();
educount++;
  $('.row-education').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">School Name</label><input type="text" class="form-control" name="schoolName[]"></div><div class="form-group col-6"><label for="End Date">School Location</label><input type="text" class="form-control" name="schoolLocation[]"></div><div class="form-group col-6"><label for="End Date">Degree</label><select class="form-control" data-live-search="true" name="degree[]"><option data-tokens="Ph.D">Ph.D</option><option data-tokens="Arts">Arts</option><option data-tokens="IT">IT</option></select></div><div class="form-group col-6"><label for="End Date">Field of Study</label><input type="text" class="form-control" name="studyField[]"></div><div class="form-group col-6"><label for="End Date">Graduation Start Date</label><input type="date" class="form-control" id="edu_start_date'+educount+'" name="graduationStart[]"></div><div class="form-group col-6"><label for="End Date">Graduation End Date</label><input type="date" class="form-control" id="edu_end_date'+educount+'" name="graduationEnd[]"><div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2"><input type="hidden" name="currently_attending[]" value="0"><input data-type="education" name="currently_attending[]" id="currentlyAttending'+educount+'" class="custom-control-input" type="checkbox" ><label for="currentlyAttending'+educount+'" class="custom-control-label">I currently attend here</label></div></div><div class="form-group col-12"><label for="End Date">Education summary</label><textarea  class="form-control"  name="education_summary[]" rows="5"></textarea></div><div class="align-self-end col-6 form-group ml-auto text-right""><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });


//job

var workcount = 0;
var ct = "{{$work_count}}";
workcount += ct;
$('.add-more-job').click(function(event){

            event.preventDefault();
            //let workingValue = $("#")
            workcount++;
  $('.add-job').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="Job Title">Job Title</label><input type="text" class="form-control" name="jobTitle[]"></div><div class="form-group col-6"><label for="Area of Work">Area of Work</label><input type="text" class="form-control" name="employer[]"></div><div class="form-group col-6"><label for="City">City</label><input type="text" class="form-control" name="whCity[]"></div><div class="form-group col-6"><label for="State">State</label><input type="text" class="form-control" name="state[]"></div><div class="form-group col-6"><label for="Start Date">Start Date</label><input type="date" id="startDate'+workcount+'" class="form-control" name="startDate[]"></div><div class="form-group col-6"><label for="End Date">End Date</label><input type="date" class="form-control" id="endtDate'+workcount+'" name="endtDate[]"><div class="custom-control custom-checkbox customCheckbox checkbox-sm mt-2"><input type="hidden" name="currentlyWorking[]" value="0"><input data-type="work" name="currentlyWorking[]" id="currentlyWorking'+workcount+'" class="custom-control-input" type="checkbox" value="1"><label for="currentlyWorking'+workcount+'" class="custom-control-label">I currently working here</label></div></div><div class="form-group col-12"><label for="End Date">Work Summary</label><textarea  class="form-control"  name="work_summary[]" rows="5"></textarea></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });

    //certificate
$('.add-more-certificate').click(function(){
  $('.certificate-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-3 row form-row"><div class="form-group col-4"><label for="End Date">Select Date</label><input type="date" class="form-control" id="certificDate" name="certificDate[]"></div><div class="form-group col-4"><label for="End Date">Certificate Name</label><input type="text" class="form-control" name="certificateName[]"></div><div class="form-group col-4"><label for="End Date">Certification Area</label><input type="text" class="form-control" name="certificateArea[]"></div><div class="form-group col-12"><label for="End Date">Certificate Summary</label><textarea  class="form-control"  name="certificate_summary[]" rows="5"></textarea></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });
    //interests
$('.add-more-interests').click(function(){
  $('.interests-row').append('<div class="border-color border-top mt-2 mx-n4 pt-4 px-2 row"><div class="form-group col-6"><label for="End Date">interests</label><input type="text" class="form-control" name="interests[]"></div><div class="align-self-end col-6 form-group ml-auto text-right"><span class="fa fa-minus-square remove text-danger"></span></div></div>');
  });
$(document).on('click', '.remove', function(){
    console.log($(this).parent(), $(this).parent().parent('.row'));
  $(this).parent().parent('.row').remove();
  });




// $('.customCheckbox input[type="checkbox"]').on('click', function () {
//   if($(this).is(':checked')){
//     $(this).parent().siblings('input[type="date"]').attr('readonly', true).val('').css('cursor', 'not-allowed');
//   }
//   else {
//     $(this).parent().siblings('input[type="date"]').removeAttr('readonly', false).css('cursor', 'text');
//   }
//  });
$(document).on('click', '.customCheckbox input[type="checkbox"]', function () {
  if($(this).is(':checked')){
    $(this).parent().siblings('input[type="date"]').attr('readonly', true).val('').css('cursor', 'not-allowed');
    $(this).siblings('input[type="hidden"]').remove();
    $(this).val(1);
  }
  else {
    $(this).parent().siblings('input[type="date"]').removeAttr('readonly', false).css('cursor', 'text');
    if($(this).data('type' == 'work')){
        $(this).append('<input type="hidden" name="currentlyWorking[]">');
    }else if($(this).data('type' == 'education')){
        $(this).append('<input type="hidden" name="currently_attending[]">');
    }
    $(this).val(0);
  }
 });



});
$(document).ready(function () {
    $('fieldset:first-of-type').fadeIn('slow');
    $('#progressbar li:first-child').addClass('active');


    $('.sign-form .required-field').on('focus', function () {
        $(this).removeClass('border-danger');
    });

    // next step
    $('.sign-form .btn-next').on('click', function () {
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;

        parent_fieldset.find('.required-field').each(function () {
            if ($(this).val() == "") {
                $(this).addClass('border-danger');
                next_step = false;
            } else {
                $(this).removeClass('border-danger');
            }
        });
        if (next_step) {
            parent_fieldset.fadeOut(400, function () {
            $(this).next().fadeIn();
            $('#progressbar li.active').next().addClass('active');
        });
        }
    });

    // previous step
    $('.sign-form .btn-previous').on('click', function () {
        $(this).parents('fieldset').fadeOut(400, function () {
            $(this).prev().fadeIn();
            $('#progressbar li.active:last').removeClass('active');
        });
    });

    // submit
    $('.sign-form').on('submit', function (e) {
        $(this).find('.required-field').each(function () {
            if ($(this).val() == "") {
                e.preventDefault();
                $(this).addClass('border-danger');
            } else {
                $(this).removeClass('border-danger');
            }
        });
    });
});
// To style all selects
$('.selectpicker').selectpicker();

// New code starts

//on change radio button
var is_full_time = false;
$(document).on('change', 'input[name=optradio]', function(){
    if($(this).val() == 'one_time'){
        $('.basic-info').addClass('d-none');
        is_full_time = false;
    }else if($(this).val() == 'full_time'){
        $('.basic-info').removeClass('d-none');
        is_full_time = true;
    }
    // $(this).attr('id', 'noLastForm');
});

$('input[name=password').focusin(function(){
    $('#password-field').children().not(".toggle-password").remove();
    $('#password-field').removeClass('is-invalid');
});

var last_form = false;
$(document).on('click', '.save-btn', function(){
    if(is_full_time == true){
        if($('#password-field').val() != $('#confirm-password').val()){
            $("#password-field").after('<span class="invalid-feedback" role="alert"><strong>The password did not match</strong>');
            $("#password-field").addClass('is-invalid');
            return false;
        }
    }
    $(this).attr('disabled', true);
    $(this).val('Please wait...');
    $('#confirmation_name').val($('.confirm-dailogue-name').val());
    $('#confirmation_password').val($('#password-field').val());
    $("#confirmation_email").val($('.confirm-dailogue-email').val());
    var full_time = is_full_time == true ? 'yes' : 'no';
    $("#is_full_time").val(full_time);
    $('.submitLastForm').click();
});

//password show and hide
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
});

$(".toggle-confirm-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
});

//Next button click each time, this function is going to call
$(document).on('submit', '.basicInfoForm', function(e) {
    e.preventDefault();
    $(this).find("button[type='submit']").prop('disabled',true);
    $(this).find('button[type=submit]').text('Please wait...');
    var parent_fieldset = $(this).parents('fieldset');
    var next_step = true;
    //execute only for first form
    if($('input[name=first_form]').val() == 'yes'){
        $(this).find('input[name=cvform_id]').val("{{ $usersEdit->id }}");
        parent_fieldset.find('.required-field').each(function () {
            if ($(this).val() == "") {
                $(this).addClass('border-danger');
                next_step = false;
            } else {
                $(this).removeClass('border-danger');
            }
        });
    }

    if(next_step == false){
        $(this).find('button[type=submit]').attr('disabled', false);
        $(this).find('button[type=submit]').html('Next');
        return false;
    }
    //This code is gonna execute only for last form
   /*  if($(this).attr('id') == 'lastForm'){

        // if last form enable the submit button
        $(this).find("button[type='submit']").prop('disabled',false);
        $(this).find('button[type=submit]').text('Done');

        $('input[name=last_form]').val('yes');
        $.ajax({
            url:"{{ url('get-basic-info') }}",
            dataType : 'json',
            type: 'get',
            data: { cvform_id : $('input[name=cvform_id]').val() },
            success: function(data){

                $('.confirm-dailogue-name').val(data.name);
                $('.confirm-dailogue-email').val(data.email);
                $('#registerModal').modal('show');
            }
        });

        //unset last form so that the control not to come this block
        $(this).attr('id', 'noLastForm');
        return false;

    } */
    // if(last_form == true){
    //     $('input[name=last_form]').val('yes');
    // }
    e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        url: "{{ url('add-basic-info') }}",
        dataType: 'json',
        type: 'post',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function(){
            $(this).find("button[type='submit']").prop('disabled',true);
            $(this).find('button[type=submit]').text('Please wait...');
        },
        success: function(result){
            $(this).find("button[type='submit']").prop('disabled',false);
            $(this).find('button[type=submit]').text('Next');
            if(result.success == true && result.last_form == true){
                window.location.href = "{{ url('users/singleUser') }}";
               /* $('.save-btn').attr('disabled', false);
                $('.save-btn').val('Submit');
                url = '{{ route("download.pdfAction", ":id") }}';
                url = url.replace(':id', result.cvform_id);
                 Swal.fire({
                    title :'Good job!',
                    text : is_full_time == true ? 'Your Account has been created!, Your credential has been sent to you via email' : 'Your Resume is ready!',
                    icon:  'success',
                    confirmButtonText: 'Ok',
                }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
                //  else if (result.isDenied) {
                //     Swal.fire('Changes are not saved', '', 'info')
                // }
                }) */
            }else{
                parent_fieldset.fadeOut(400, function () {
                    $(this).next().fadeIn();
                    $(this).next().find('input[name=cvform_id]').val(result.cvform_id);
                    $('#progressbar li.active').next().addClass('active');
                });
            }
        }
      });

});

//new code ends
</script>
@stop

