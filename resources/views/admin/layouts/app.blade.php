
@include('admin.includes.head')

<div class="wrapper">
     @include('admin.includes.header')
     <div class="main-content">
     @include('admin.includes.sidebar')

    <div class="right-content">
     @yield('content')
    </div>
      </div>
      @include('admin.includes.footer')

</div>


<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();

} );


(function($) {
  'use strict';
  $(function() {
    var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
    $('.nav-item').each(function() {
      var $this = $(this);
      if (current === "") {
        //for root url
        if ($this.find(".nav-link").attr('href').indexOf("index.html") !== -1) {
          $(this).find(".nav-link").parents('.nav-item').last().addClass('active');
          $(this).addClass("active");
        }
      } else {
        //for other url
        if ($this.find(".nav-link").attr('href').indexOf(current) !== -1) {
          $(this).find(".nav-link").parents('.nav-item').last().addClass('active');
          $(this).addClass("active");
        }
      }
    })
  });
})
(jQuery);

</script>
</body>
</html>












{{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" ></script>
 --><script src="{{ asset('/assets/js/bootstrap-datepicker.min.js') }}" ></script>
{{--
{{-- Toastr Plugin
<script  src="{{asset('assets/front/js/toastr.min.js')}}"></script>

{{-- sweetalert2 plugin
<script  src="{{asset('assets/front/js/sweetalert2.all.min.js')}}"></script>
<script  src="{{asset('assets/front/js/sweetalert2.min.js')}}"></script> --}} 

<script>

    // $(document).ready(function(){
    //     const Swal = require('sweetalert2');
    // });

</script>
