@extends('admin.layouts.app')

@section('content')
 <div class="bg-white main-titl-row  pb-3 pt-3 row align-items-center ">

    <div class="d-md-none h-search-col search-col col-12 collapse" id="searchsection">
     <div class="input-group custom-input-group mb-3" >
          <input type="text" class="form-control" placeholder="Search">
          <div class="align-items-center input-group-prepend">
            <span class="input-group-text fa fa-search"></span>
          </div>
     </div>
    </div>

  <div class="col-lg-8 col-md-7 col-sm-7  title-col">
    <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Contact Us update List</h3>
  </div>
 {{--  <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
      <button type="button" class="btn btn-primary" data-toggle="modal" href='#add_new_doctor'>Add new Doctor</button>
  </div> --}}

</div>
	<div class="container my-5">
		<div class="row">
			<div class="col-md-8">
				<form   action="{{ route('contact-us.update',$contactUs->id) }}" method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="Full Name">Full Name</label>
						<input type="text" name="fullname" value="{{$contactUs->full_name }}" class="form-control">
					</div>
					<div class="form-group">
						<label for="Phone No">Phone No</label>
						<input type="tel" name="phone" value="{{$contactUs->phone }}" class="form-control">
					</div>
					<div class="form-group">
						<label for="Email">Email</label>
						<input type="email" name="email" value="{{$contactUs->email }}" class="form-control">
					</div>
					<div class="form-group">
						<label for="subject">Subject</label>
						<input type="text" name="subject" value="{{$contactUs->subject }}" class="form-control">
					</div>
					<div class="form-group">
					<label for="message">Message</label>
					<textarea type="text" name="message" cols="15" rows="5"  class="form-control">{{$contactUs->message}}</textarea>
					</div>
					<input type="submit" name="submit" value="Contact Us" class="btn btn-success">
				</form>
			</div>
			<div class="col-md-4">
				{{-- <ul class="list-unstyled">
					<li><em class="fa fa-phone"></em> 00923119777099</li>
					<li><em class="fa fa-envelope"></em> Cv@gmail.com</li>
				</ul> --}}
			</div>
		</div>
	</div>
@stop