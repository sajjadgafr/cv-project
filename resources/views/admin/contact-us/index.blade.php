@extends('admin.layouts.app')

@section('content')
 <div class="bg-white main-titl-row  pb-3 pt-3 row align-items-center ">

    <div class="d-md-none h-search-col search-col col-12 collapse" id="searchsection">
     <div class="input-group custom-input-group mb-3" >
          <input type="text" class="form-control" placeholder="Search">
          <div class="align-items-center input-group-prepend">
            <span class="input-group-text fa fa-search"></span>
          </div>
     </div>
    </div>

  <div class="col-lg-8 col-md-7 col-sm-7  title-col">
    <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Contact Us List</h3>
  </div>
 {{--  <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
      <button type="button" class="btn btn-primary" data-toggle="modal" href='#add_new_doctor'>Add new Doctor</button>
  </div> --}}

</div>
<div class="row">
  <div class="col-lg-12 col-12 appointment-col">
    <div class="bg-white custompadding customborder">
      <div class="section-header">
       <h5 class="mb-1">Contact List</h5>
      </div>
      <div class="table-responsive">
        <table id="example" class="table table-bordered table-striped tabel-align-middle" style="width:100%">
          <thead>
              <tr>
                  <th>Full Name</th>
                  <th>Phone No</th>
                  <th>Email</th>
                  <th>Subject</th>
                  <th>Message</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>

          	@foreach($contactUs as $contact)
              <tr>
                  
                  <td>{{$contact->full_name}}</td>
                  <td>{{$contact->phone}}</td>
                  <td>{{$contact->email}}</td>
                  <td>{{$contact->subject}}</td>
                  <td>{{$contact->message}}</td>
                  
                  <td><div class="d-flex text-center">
                      <a href="{{ route('contact-us.edit',$contact->id ) }}" class="actionicon bg-success editaction">
                        <i class="fa fa-pencil"></i></a> 
                      <br>
                      <a href="#" class="contacUstBtn btn actionicon bg-info editaction"
                      data-target=".contactModal" data-toggle="modal" data-id="{!! $contact->id !!}"><i class="fa fa-eye"></i></a> 
                      <br />
                      <form method="POST" action="{{ route('contact-us.destroy',$contact->id ) }}">
                          @method('DELETE')
                         @csrf
                    <button type="submit" name="submit" onclick="return confirm('Are you sure you want to delete this item?'); "
                      class="actionicon bg-danger deleteaction"><i class="fa fa-trash"></i></button>      
                    </form>
                    </div></td>
              </tr>

            @endforeach
          </tbody>
      </table>
    </div>
   </div>
  </div>
			


              <!-- Owner Model -->

            <div  class="modal fade bs-example-modal-sm contactModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header" style="background:linear-gradient(to right,#226faa 0,#2989d8 37%,#72c0d3 100%)">
                <h5 class="modal-title" id="exampleModalLabel">Onwer Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <table id="example2" class="table table-bordered ">
                <thead>
                  <tr>
                    <th>Full Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Subject</th>
                    <th>Message</th>
                  </tr>
                </thead>
                <tbody>
                  <tbody>
                    <tr>
                        <td><div class="fullName"></div></td>  
                        <td><div class="phone"></div></td>  
                        <td><div class="email"></div></td>  
                        <td><div class="subject"></div></td>  
                        <td><div class="message"></div></td>  
                    </tr>
                  </tbody>
                </table>             
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
              <div class="text-center mt-1" align="content"></div>
            <!-- /.card-body -->
          </div>
</div>

<script>

$(document).ready(function(){
  $(".contacUstBtn").click(function(){
    var id=$(this).data('id');
     // alert(id);

    $('#form_result').html('');
    $.ajax({
        type : 'GET',
        url:'/contactShow/'+id,
        dataType:'JSON',
        data : {'id':id},

        success:function(html){
          // console.log(html.data.owner_emirates); 
          
          $('.fullName').html("<p>"+html.data.full_name+"</p>");
          $('.phone').html("<p>"+html.data.phone+"</p>");
          $('.email').html("<p>"+html.data.email+"</p>");
          $('.subject').html("<p>"+html.data.subject+"</p>");
          $('.message').html("<p>"+html.data.message+"</p>");
        
        }
      });

    });
});

	
</script>
@stop
