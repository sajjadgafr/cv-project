@include('includes.header')

	<div class="container my-5">
		<div class="row">
			<div class="col-md-8">
				<form   action="{{ route('contact-us.store')}}" method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="Full Name">Full Name</label>
						<input type="text" name="fullname" class="form-control">
					</div>
					<div class="form-group">
						<label for="Phone No">Phone No</label>
						<input type="tel" name="phone" class="form-control">
					</div>
					<div class="form-group">
						<label for="Email">Email</label>
						<input type="email" name="email" class="form-control">
					</div>
					<div class="form-group">
						<label for="subject">Subject</label>
						<input type="text" name="subject" class="form-control">
					</div>
					<div class="form-group">
						<label for="message">Message</label>
						<textarea type="text" name="message" cols="15" rows="5"  class="form-control"></textarea>
					</div>
					<input type="submit" name="submit" value="Contact Us" class="btn">
				</form>
			</div>
			<div class="col-md-4">
				<ul class="list-unstyled">
					<li><em class="fa fa-phone"></em> 00923119777099</li>
					<li><em class="fa fa-envelope"></em> Cv@gmail.com</li>
				</ul>
			</div>
		</div>
	</div>

@include('includes.footer')