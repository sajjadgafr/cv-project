@extends('admin.layouts.app')
@section('title', 'Roles')
@section('content')
    <div class="main-titl-row pt-3 row align-items-center">
        <div class="col-lg-8 col-md-7 col-sm-7  title-col">
            <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Roles</h3>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-5 title-right-col text-right">
            <a href="{{ Route('role.create') }}" class="btn" >Add new User</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-12 appointment-col">
            <div class="bg-white custompadding customborder">
                 {{-- <div class="section-header">
                 <h5 class="mb-1">User List</h5>
                </div> --}}
                {{-- <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
                    <button type="button" class="btn" data-toggle="modal" href='#add_new_doctor'>Add new Doctor</button>
                </div> --}}
                <div class="table-responsive">
                    <table id="example" class="table tabel-align-middle cvuserslist">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($user as $profile)
                            <tr>
                                <td>{{ $profile->name}}</td>
                                <td>{{ $profile->email}}</td>
                                <td>@if($profile->role == 1)
                                        <span class="badge badge-success">Admin</span>
                                    @elseif($profile->role == 0)
                                        <span class="badge badge-success">User</span>
                                    @endif
                                </td>

                                <td><div class="d-flex justify-content-center text-center">
                                        <a href="{{ route('role.show',$profile->id) }}" title="Preview Profile" class="actionicon border border-success editaction fa fa-eye text-success"></a>
                                        <a href="{{ route('role.edit', $profile->id) }}"  title="Edit Profile"  class="actionicon border border-info editaction fa fa-pencil text-info"></a>

{{--                                        <form method="POST" action="{{ route('users.destroy', ['id' => $user->id] )}}">--}}
{{--                                            @method('DELETE')--}}
{{--                                            @csrf--}}
{{--                                            <button type="submit" name="submit" onclick="return confirm('Are you sure you want to delete this item?'); "--}}
{{--                                                    class="actionicon bg-white border border-danger deleteaction fa fa-trash text-danger"></button>--}}
{{--                                        </form>--}}
                                    </div></td>
                            </tr>

                        @endforeach


                        </tbody>
                    </table>


                    </div>
                </div>
            </div>
        </div>


@stop
