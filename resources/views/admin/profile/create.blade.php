    @extends('admin.layouts.app')
    @section('title', 'Roles')
    @section('content')
        <div class="main-titl-row pt-3 row align-items-center">
            <div class="col-lg-8 col-md-7 col-sm-7  title-col">
                <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Roles Create</h3>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-5 title-right-col text-right">
                <a  class="btn" href='{{ route('role.index') }}'>Back</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-12 appointment-col">
                <div class="bg-white custompadding customborder">
                    {{-- <div class="section-header">
                    <h5 class="mb-1">User List</h5>
                   </div> --}}
                    {{-- <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
                        <button type="button" class="btn" data-toggle="modal" href='#add_new_doctor'>Add new Doctor</button>
                    </div> --}}

    <div class="box-body">
        <form method="post" action="{{ Route('role.store') }}">
            @csrf
        <div class="form-group">
            <label for="">Name:</label>

            <input placeholder="Name" class="form-control form-control-user" name="name" type="text">
        </div>
        <div class="form-group">
            <label for="">Email:</label>
            <input placeholder="Email" class="form-control" name="email" type="email">
        </div>
        <div class="form-group">
            <label for="">Password:</label>
            <input placeholder="Password" class="form-control form-control-user" name="password" type="password" value="">
        </div>
        <div class="form-group">
            <label for="">Confirm Password:</label>
            <input placeholder="Confirm Password" class="form-control" name="confirm-password" type="password" value="">
        </div>
        <div class="form-group">
            <label for="">Role:</label>
            <select class="form-control"  name="role">
                <option >Select</option>
                <option value="0">User</option>
                <option value="1">Admin</option>
            </select>
        </div>


    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <button type="submit" class="btn ">Submit</button>
    </div>
</form>

</div>
@stop
