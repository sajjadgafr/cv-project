@extends('admin.layouts.app')
@section('title', 'Roles')
@section('content')
    <div class="main-titl-row pt-3 row align-items-center">
        <div class="col-lg-8 col-md-7 col-sm-7  title-col">
            <h3 class="fontbold maintitle mb-2 mb-sm-0 text-capitalize">Roles Create</h3>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-5 title-right-col text-right">
            <a class="btn" href='{{ Route('role.index') }}'>Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-12 appointment-col">
            <div class="bg-white custompadding customborder">
                {{-- <div class="section-header">
                <h5 class="mb-1">User List</h5>
               </div> --}}
                {{-- <div class="col-lg-4 col-md-5 col-sm-5 search-col text-right">
                    <button type="button" class="btn" data-toggle="modal" href='#add_new_doctor'>Add new Doctor</button>
                </div> --}}
<div class="box-body">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{$role->name}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Email:</strong>
            {{ $role->email }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Roles:</strong>
            @if($role->role == 1)
                <label class="badge badge-success">Admin</label>
            @elseif($role->role == 0)
                <label class="badge badge-success">User</label>
            @endif
        </div>
    </div>
</div>
            </div></div>
    </div>
    @stop
