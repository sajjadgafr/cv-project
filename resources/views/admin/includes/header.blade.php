
<header class="header fixed-top">

<div class="toprow">
 <figure class="align-items-center justify-content-center d-flex logo">
  <a href="index.html">
    <img class="img-fluid" src="{{ asset('/assets/img/logo.png') }}">
    {{-- <img src="{{ asset('/admin/assets/img/dashboard-logo.png') }}" alt="logo" class="img-fluid lg-logo">
    <img src="{{ asset('/admin/assets/img/sm-logo.png') }}" alt="logo" class="img-fluid sm-logo"> --}}

  </a>
</figure>
<div class="top-bar align-items-center d-flex justify-content-between">
  <div class="align-items-center col-3 col-lg-5 col-md-6 col-sm-2 d-flex pl-0 header-icons">
  <div class="backlink mr-3">
    <a href="javascript:avoid(0)" class="menuarrow fa fa fa-bars"></a>
  </div>
   <a href="{{ route('webiste.gotowebiste') }}"  class="btn" target="_blank">Go to Website</a>
  </div>


  <!-- Header User Info Start Here -->
  <div class="col-lg-4 col-sm-10 col-md-11 col-9 userlinks">
    <ul class="align-items-center justify-content-end d-flex list-unstyled mb-0 userlist">
    <li class="nav-item dropdown userdropdown settingdropdown">
      <a class="align-items-center d-flex nav-link dropdown-toggle fa fa-cog" href="#" id="usermessages" data-toggle="dropdown">
      </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#"><span class="dropdown-icon oi oi-person"></span> Profile</a>
          <a class="dropdown-item" href="#">
            <span class="dropdown-icon oi oi-account-logout"></span> Logout
          </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Help Center</a>
          <a class="dropdown-item" href="#">Ask Forum</a>
          <a class="dropdown-item" href="#">Keyboard Shortcuts</a>
        </div>
    </li>
    <li class="nav-item dropdown userdropdown ntifction-dropdown">
       <a class="align-items-center d-flex nav-link dropdown-toggle  fa fa-bell-o" href="#" id="usernotification" data-toggle="dropdown" aria-expanded="true">
        <!-- <img src="assets/img/notification.png" alt="notification image" class="img-fluid"> -->
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="usercol notinfo">
          <a href="#" class="notiflink">
            <div class="notifi-name fontbold">Jessica Caruso</div>
            <div class="notifi-desc">accepted your invitation to join the team.</div>
            <span class="notifi-date fontmed">2 min ago</span>
          </a>
         </div>
         <div class="usercol notinfo">
           <a href="#" class="notiflink">
            <div class="notifi-name fontbold">Jessica Caruso</div>
            <div class="notifi-desc">accepted your invitation to join the team.</div>
            <span class="notifi-date fontmed">2 min ago</span>
          </a>
         </div>
      </div>
    </li>
    <li class="nav-item dropdown prof-dropdown">
      <a class="align-items-center d-flex dropdown-toggle nav-link profilelink" href="#" id="profilelink" data-toggle="dropdown">
        <span class="username text-center">{{ Auth::user()->name }} <small class="d-block">{{  Auth::user()->role == 0 ? 'User':'Admin' }}</small></span>
        @php
        if(Auth::user()->cvform){
            if(Auth::user()->cvform->avatar){
                $img =Auth::user()->cvform->avatar;
            }else{
                $img = 'assets/img/avatar.png';
            }
        }else{
            $img = 'assets/img/avatar.png';
        }
        @endphp
        <img src="{{ asset('/'.@$img) }}" alt="user image" class="img-fluid rounded-circle img-fluid profileimg">
      </a>
      <div class="dropdown-menu">
          <a class="dropdown-item" href="user-profile.html">
            <span class="dropdown-icon oi oi-person"></span> Profile
          </a>
          <a  href="{{ route('logout') }}" class="dropdown-item"
           onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"> {{ __('Logout') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </div>
    </li>
  </ul>
</div>
<!-- Header User Info End Here -->
</div>
</div>
</header>
