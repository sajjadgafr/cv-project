<aside class="sidebar color-white">
<div class="sidebarbg"></div>

<div class="sidebarprof text-center pt-md-4 pt-3 pr-3 pl-3">
    @php
    if(Auth::user()->cvform){
        if(Auth::user()->cvform->avatar){
            $img =Auth::user()->cvform->avatar;
        }else{
            $img = 'assets/img/avatar.png';
        }
    }else{
        $img = 'assets/img/avatar.png';
    }
    @endphp
  <img src="{{ asset('/'.@$img) }}" alt="user image" class="img-fluid rounded-circle img-fluid sideprofileimg">
  <div class="sideprotext">
  <h6 class="mb-1">{{ Auth::user()->name }}</h6>
  <span class="d-block">{{ Auth::user()->role == 0 ? 'User':'Admin'}}</span>
  </div>
</div>

<nav class="navbar sidebarnav navbar-expand-sm pt-md-4 pt-3">

<!-- Links -->
<ul class="menu w-100 list-unstyled">
    <li class="nav-item">
      <a class="nav-link" href="{{ route('dashboard.index') }}">
        <i class="fa fa-home"></i> <span>Dashboard</span></a>
    </li>

    @if(Auth::user()->role == 0)
        <li class="nav-item">
            <a class="nav-link" href="{{route('users.singleUser')}}">
            <i class="fa fa-user"></i> <span>Your Cv</span></a>
        </li>
    @endif
    @if(Auth::user()->role == 1)

    <li class="nav-item">
        <a class="nav-link" href="{{ route('role.index') }}">
            <i class="fa fa-cog"></i> <span>Roles</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('users.index')}}">
        <i class="fa fa-user"></i> <span>Cv Users</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route('contact-us.index') }}">
        <i class="fa fa-users"></i> <span>Contact Us</span></a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="#">
        <i class="fa fa-cog"></i> <span>Setting</span></a>
    </li>
    @endif

  </ul>
</nav>
</aside>
