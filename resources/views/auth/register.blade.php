@include('auth.includes.header')
<div class="absolute d-flex h-100 loginwrapper ml-0 mr-0 row w-100">
  <div class="align-items-center d-md-flex login-left text-center text-white d-none justify-content-center">
    <div class="login-left-bg">
      <img src="assets/img/logo.png" class="d-block img-fluid mx-auto mb-4" alt="logo">
      <h2>Create a Free CV</h2>
    </div>
  </div>
  <div class="login-right align-items-center d-flex ml-auto  ml-auto mr-auto justify-content-center">
    <div class="login-right-bg w-100 h-100 d-flex flex-column">
      <div class="dont-have-account text-right">
        <span class="d-inline-block mr-2 pr-1">Already have an account?</span> <a href="{{ route('login') }}" class="btn btn-custom btn-sm">Sign In</a>
      </div>
      <div class="login-form my-auto">
        <!-- <h2 class="fontbold text-uppercase welcometitle">Welcome</h2>
        <img src="assets/img/logo.png" class="d-block img-fluid login-form-logo" alt="logo"> -->
        <h4 class="fontbold text-uppercase">Register</h4>
        <p>Enter your details below.</p>


        <form method="POST" action="{{ route('register') }}" class="mt-4">
                @csrf


                  <div class="form-group mb-3">
                            <label for="name" >{{ __('Name') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="email" >{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="password" >{{ __('Password') }}</label>

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>

                       <div class="align-items-center d-flex form-group justify-content-between mb-0 mt-4 pt-2">
                        <input type="submit" class="btn btn-custom login-btn text-capitalize fontbold" value="Register">
                        <span class="loginwith pl-2 pr-2 text-capitalize">or Already have an account? <a href="{{ route('login') }}" class="btn-link font-weight-bold">Sign In</a></span>
                        <!-- <ul class="d-flex list-unstyled social-menu mb-0">
                          <li><a href="#" class="fa fa-facebook"></a></li>
                          <li><a href="#" class="fa fa-google-plus"></a></li>
                        </ul> -->
                      </div>
          </form>
      </div>
    </div>
  </div>

</div>



<!-- 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" >{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" >{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" >{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" >{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@include('auth.includes.footer')
