@include('auth.includes.header')
<div class="absolute d-flex h-100 loginwrapper ml-0 mr-0 row w-100">
  <div class="align-items-center d-md-flex login-left text-center text-white d-none justify-content-center">
    <div class="login-left-bg">
      <img src="{{ asset('/assets/img/logo.png') }}" class="d-block img-fluid mx-auto mb-4" alt="logo">
      <!-- <h4>lorem ipsum dolor sit amet</h4>
      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
      <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
    </div>
  </div>
  <div class="login-right align-items-center d-flex ml-auto  ml-auto mr-auto">
    <div class="login-right-bg w-100">
      <div class="login-form">
        <!-- <img src="{{ asset('/assets/img/logo.png') }}" class="d-block img-fluid login-form-logo" alt="logo"> -->
        <h4 class="fontbold text-uppercase">Reset Password</h4>

        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}" class="mt-4">
                        @csrf

                        <div class="form-group ">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group mb-0">
                            <input type="submit" class="btn btn-custom login-btn text-capitalize fontbold" value="Send Password Reset Link">
                        </div>
                    </form>


      </div>
    </div>
  </div>

</div>

@include('auth.includes.footer')