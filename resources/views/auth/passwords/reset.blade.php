@include('auth.includes.header')


<div class="absolute d-flex h-100 loginwrapper ml-0 mr-0 row w-100">
  <div class="align-items-center d-md-flex login-left text-center text-white d-none justify-content-center">
    <div class="login-left-bg">
      <img src="{{ asset('/assets/img/logo.png') }}" class="d-block img-fluid mx-auto mb-4" alt="logo">
      <h4>lorem ipsum dolor sit amet</h4>
    </div>
  </div>
  <div class="login-right align-items-center d-flex ml-auto  ml-auto mr-auto">
    <div class="login-right-bg w-100">
<!--       <div class="dont-have-account text-right">
        <span class="d-inline-block mr-2 pr-1">Don't have an account?</span> <a href="{{ route('register') }}" class="btn btn-custom">sign up</a>
      </div> -->
      <div class="login-form">
<!--         <h2 class="fontbold text-uppercase welcometitle">Welcome</h2> -->
        <img src="{{ asset('/assets/img/logo.png') }}" class="d-block img-fluid login-form-logo" alt="logo">
        <h4 class="fontbold text-uppercase">Reset Password</h4>
        <p>Enter your Detail below.</p>


        <form method="POST" action="{{ route('password.update') }}" class="mt-4">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row">
                            <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>


                       <div class="form-group mb-0">
                        <input type="submit" class="btn btn-custom login-btn text-uppercase fontbold" value="Reset Password">
                      </div>
          </form>
      </div>
    </div>
  </div>

</div>

@include('auth.includes.footer')