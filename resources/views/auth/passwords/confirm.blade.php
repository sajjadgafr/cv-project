@include('auth.includes.header')
<div class="absolute d-flex h-100 loginwrapper ml-0 mr-0 row w-100">
  <div class="align-items-center d-md-flex login-left text-center text-white d-none justify-content-center">
    <div class="login-left-bg">
      <img src="{{ asset('/assets/img/logo.png') }}" class="d-block img-fluid mx-auto mb-4" alt="logo">
    </div>
  </div>
  <div class="login-right align-items-center d-flex ml-auto  ml-auto mr-auto">
    <div class="login-right-bg w-100">
      <div class="login-form">
        <img src="{{ asset('/assets/img/logo.png') }}" class="d-block img-fluid login-form-logo" alt="logo">
        <h4 class="fontbold text-uppercase">Confirm Password</h4>

         <p>{{ __('Please confirm your password before continuing.') }}</p>

                 <form method="POST" action="{{ route('password.confirm') }}" class="mt-4">
                        @csrf
                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="align-items-center d-flex form-group justify-content-between mb-0">
                        <input type="submit" class="btn btn-custom login-btn text-capitalize fontbold" value="Confirm Password">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                      </div>
                </form>
      </div>
    </div>
  </div>

</div>

@include('auth.includes.footer')


