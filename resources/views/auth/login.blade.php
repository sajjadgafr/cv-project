@include('auth.includes.header')


<div class="absolute d-flex h-100 loginwrapper ml-0 mr-0 row w-100">
  <div class="align-items-center d-md-flex login-left text-center text-white d-none justify-content-center">
    <div class="login-left-bg">
      <img src="assets/img/logo.png" class="d-block img-fluid mx-auto mb-4" alt="logo">
      <h2>Welcome</h2>
      <!-- <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h4>
      <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
    </div>
  </div>
  <div class="login-right align-items-center d-flex ml-auto  ml-auto mr-auto justify-content-center">
    <div class="login-right-bg w-100 h-100 d-flex flex-column">
      <div class="dont-have-account text-right">
        <span class="d-inline-block mr-2 pr-1">Don't have an account?</span> <a href="{{ route('register') }}" class="btn btn-custom btn-sm">sign up</a>
      </div>
      <div class="login-form my-auto">
<!--         <h2 class="fontbold text-uppercase welcometitle">Welcome</h2>
        <img src="assets/img/logo.png" class="d-block img-fluid login-form-logo" alt="logo"> -->
        <h4 class="fontbold text-uppercase">Login</h4>
        <p>Enter your details below.</p>


        <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn btn-lg btn-success btn-block">
            <strong>Login With Google</strong>
          </a>

          <a class="btn btn-lg btn-primary btn-block" href="{{ url('login/facebook') }}">
 <strong>Login With Facebook</strong>
 </a>


        <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('/login') }}" class="mt-4">
                @csrf
          <div class="form-group">
              <label for="email">Username</label>
              <input type="text" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
          </div>
          <div class="form-group mb-3">
             <label for="pwd">Password</label>
             <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="pwd" name="password">
              @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
          <!-- <div class="form-group mb-3">
           {{--  @captcha
            @if ($errors->has('g-recaptcha-response'))
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                  </span>
            @endif --}}
          </div> -->

          <div class="form-group justify-content-between mb-3 mb-md-4 pb-2 row">
            <div class="col-6 checkbox">
               <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="custom-control-label text-capitalize" for="remember">remember me</label>
              </div>
            </div>
             <div class="col-6 forgetpassrd text-right">
              <a href="{{ route('password.request') }}" class="btn-link">Forgot Password?</a>
            </div>
          </div>
           <div class="align-items-center d-flex form-group justify-content-between mb-0">
            <input type="submit" class="btn btn-custom login-btn text-capitalize fontbold" value="login">
            <span class="loginwith pl-2 pr-2 text-capitalize">Don't have an account? <a href="{{ route('register') }}" class="btn-link font-weight-bold">Sign In</a></span>
            <!-- <ul class="d-flex list-unstyled social-menu mb-0">
              <li><a href="#" class="fa fa-facebook"></a></li>
              <li><a href="#" class="fa fa-google-plus"></a></li>
            </ul> -->
          </div>
          </form>
      </div>
    </div>
  </div>

</div>


@include('auth.includes.footer')
