<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
   
      @if($cvform->avatar)
         <div class="img">
          <img src="{{ asset( $cvform->avatar )}}" alt="user image" style="widows: 20%;">      
        </div>
      @endif
   
    <table>
    <thead>
      <tr>
        
        <th><b>First Name</b></th>
        <th><b>Last Name</b></th>
        <th><b>Profession</b></th>
        <th><b>Street Address</b></th>
        <th><b>state Province</b></th>
        <th><b>city</b></th>
        <th><b>zipCode</b></th>
        <th><b>phone</b></th>
        <th><b>email</b></th>
        <th><b>summary</b></th>
        <th><b>date_of_birth</b></th>
        <th><b>marital_status</b></th>
        <th><b>id_card</b></th>
        <th><b>AdditionalNformation</b></th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td>{{$cvform->firstName}}</td>
        <td>{{$cvform->lastName}}</td>
        <td>{{$cvform->profession}}</td>
        <td>{{$cvform->streetAddress}}</td>
        <td>{{$cvform->stateProvince}}</td>
        <td>{{$cvform->city}}</td>
        <td>{{$cvform->zipCode}}</td>
        <td>{{$cvform->phone}}</td>
        <td>{{$cvform->email}}</td>
        <td>{{$cvform->summary}}</td>
        <td>{{$cvform->date_of_birth}}</td>
        <td>{{$cvform->marital_status}}</td>
        <td>{{$cvform->id_card}}</td>
        <td>{{$cvform->AdditionalNformation}}</td>
      </tr>
      </tbody>
    </table>




<?php echo "-------------------------------"; ?>
       <h4 >Social Link</h4>
     <table>
      <tbody>
        <tr>
           @foreach($social as $social_links)
            <td>{{$social_links->sociallinkName}}-{{$social_links->socialLink}}</td>
           @endforeach
        </tr>
      </tbody>
    </table>


<?php echo "-------------------------------"; ?>
       <h4 >lANGUAGEG</h4>

    <table>
      <tbody>
        @foreach($language as $lang)
        <tr>
          <td>{{$lang->chooseLanguage}}</td>
          <td>{{$lang->ProficiencyLanguage}}</td>
        </tr>       
        @endforeach
      </tbody>
    </table>


    <?php echo "-------------------------------"; ?>
       <h4 >interests</h4>

    <table>
      <tbody>
        @foreach($interest as $interests)
        <tr>
          <td>{{$interests->interests}}</td>
        </tr>       
        @endforeach
      </tbody>
    </table>


<?php echo "-------------------------------"; ?>

        <h4 >certificate</h4>

    <table>
      <tbody>
        @foreach($certificat as $certi)
        <tr>
          <td>{{$certi->certificateName}}</td>
          <td>{{$certi->certificate_area}}</td>
          <td>{{$certi->certificDate}}</td>
          <td>{{$certi->certificate_summary}}</td>
        </tr>       
        @endforeach
      </tbody>
    </table>

    <?php echo "-------------------------------"; ?>

        <h4 >skill</h4>

     @foreach($skill as $skills)
      <ul>
        <li >{{$skills->skill}} - <span>{{$skills->skillProgress}}</span></li>
      </ul>
      @endforeach

      <?php echo "-------------------------------"; ?>

        <h4 >education</h4>

        @foreach($education as $edu)
          <li>
            <h6 >{{$edu->studyField}}</h6>
            <span>{{$edu->degree}}</span>
            <span >{{$edu->schoolName}}</span>
            <span >{{$edu->schoolLocation}}</span>
            <span >{{$edu->graduationStart}} - {{$edu->graduationEnd}}</span>
          </li>
        @endforeach


        <?php echo "-------------------------------"; ?>

        <h4 >Work history</h4>
            <ul >
              @if(!empty($workHistory))
              @foreach($workHistory as $work)
              <li>
                <h6 >{{$work->jobTitle}}</h6>
                 @if(!empty($work))
                <span >{{$work->employer}}</span>
                <span >{{$work->startDate}} - {{$work->endtDate}}</span>
                @endif
                <p>{{$work->work_summary}}</p>
              </li>
              @endforeach
               @endif
            </ul> 

  </body>
</html>
