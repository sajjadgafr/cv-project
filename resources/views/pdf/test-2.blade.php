<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr>
        <td><b>First Name</b></td>
        <td><b>Last Name</b></td>
        <td><b>Email</b></td>     
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$data->firstName}}</td>
        <td>{{$data->lastName}}</td>
        <td>{{$data->email}}</td>
      </tr>
      </tbody>
    </table>
  </body>
</html>