
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CV</title>
    <!-- FontAwesome css -->
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="style.css">
    <style>

/* @media print {
  body {
    font-size: 12px;
    font-family: 'Times New Roman'
  }
 }


@page {
    font-size: 12px;
  /* margin: 0px 20px 0px 20px;
}

body{
    font-size:8pt;
} */
    </style>



  </head>
  <body>
<style type="text/css">
    .cv-left-bg { background: #ccc; }
</style>

    <div class="container">
        <div class="row">
            <div class="col-4 cv-left-col">
              <div class="cv-left-bg mx-0 row  h-100">
                  <figure class="col-12 text-center mb-0 p-3">
                     <!--  @if(!empty($usersInfo->avatar))
                            <img src="{{ asset( $usersInfo->avatar )}}" alt="user image" class="img-fluid img-thumbnail w-100">
                        @else
                            <img src="{{ asset('/admin/assets/img/profile-placeholder.png') }}" alt="user image" class="img-fluid img-thumbnail w-100">
                        @endif -->
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRKmSJwmrJuiFy3u2KT3F-_r95RYdTJ6bcdkg&usqp=CAU" alt="user image" class="img-fluid img-thumbnail w-100" style="max-height: 120px;">

                  </figure>
                  <div class="col-12 mb-3">
                    <h5 class="font-weight-bold mb-3 text-capitalize">Contact Us</h5>
                      <ul class="list-unstyled skillslist mb-0">
                          <li>{{$usersInfo->phone}}</li>
                          <li>{{$usersInfo->email}}</li>
                          <li>{{$usersInfo->streetAddress}}</li>
                          <li>{{$usersInfo->city}}</li>
                          <li>{{$usersInfo->date_of_birth}} </li>
                          <li>{{$usersInfo->marital_status}}</li>
                          <li>{{$usersInfo->id_card}} </li>
                      </ul>
                  </div>

                  <div class="col-12 mb-3">
                    <h5 class="font-weight-bold mb-3 text-capitalize">Social Links</h5>
                    <ul class="list-unstyled sociallinks">
                        @foreach($social as $social_links)
                            <li class="mb-3 d-flex align-items-center"><em class=" text-white rounded-circle text-center mr-2 fa fa-{{$social_links->sociallinkName}}"></em> <span>{{$social_links->socialLink}}</span></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-12 mb-3">
                    <h5 class="font-weight-bold mb-3 text-capitalize">Languages</h5>
                    <table class="table table-borderless table-striped">
                        <tbody>
                        @foreach($language as $lang)
                            <tr>
                                <td class="w-100">{{$lang->chooseLanguage}}</td>
                                <td>{{$lang->ProficiencyLanguage}}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-12 mb-3">
                    <h5 class="font-weight-bold mb-3 text-capitalize">interests</h5>
                    <ul class="list-unstyled skillslist mb-0">
                        @foreach($interest as $interests)
                            <li class="list-inline-item mb-2"><span class="badge badge-pill badge-light border font-weight-normal px-3 py-2">{{$interests->interests}}</span></li>
                        @endforeach
                    </ul>
                </div>


              </div>
            </div>
            <div class="col-8 cv-left-col">
                <div class="row">
                    <div class="col-12 mb-4">
                    <div class="profilename bg-primary">
                    <h3 class="font-weight-bold">{{$usersInfo->firstName}} {{$usersInfo->lastName}}</h3>
                    <p class="mb-2">{{$usersInfo->profession}}</p>
                    <p class="mb-2">{{$usersInfo->streetAddress}} {{$usersInfo->city}}</p>
                    </div>
                    </div>

                    @if(!empty($usersInfo->summary))
                    <div class="col-12 mb-3">
                        <h4 class="font-weight-bold text-capitalize">Summary</h4>
                        <p>{{$usersInfo->summary}}</p>
                    </div>
                     @endif
                     <div class="col-12 mb-3">
                        <h4 class="font-weight-bold text-capitalize">Work history</h4>
                        <ul class="list-unstyled profileSectList">
                            @if(!empty($workHistory))
                                @foreach($workHistory as $work)
                                    <li>
                                        <h6 class="font-weight-bold">{{$work->jobTitle}}</h6>
                                        @if(!empty($work))
                                            <div class="d-flex justify-content-between mb-3">
                                                <span class="history-company"><em class="fa fa-building mr-2"></em>{{$work->employer}}</span>
                                                <span class="history-start-end"><em class="fa fa-calendar mr-2"></em>{{$work->startDate}} - {{$work->endtDate}}</span>
                                            </div>
                                        @endif
                                        <p>{{$work->work_summary}}</p>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="col-12 mb-3">
                    <h4 class="font-weight-bold text-capitalize">Education</h4>
                    <ul class="list-unstyled profileSectList">
                        @foreach($education as $edu)
                            <li>
                                <h6 class="font-weight-bold">{{$edu->studyField}}</h6>
                                <div class="d-flex justify-content-between mb-3">
                                    <span class="edu-start-end mr-2"><em class="fa fa-graduation-cap mr-2"></em>{{$edu->degree}}</span>
                                    <span class="edu-area mr-2"><em class="fa fa-building mr-2"></em>{{$edu->schoolName}}</span>
                                    <span class="edu-location mr-2"><em class="fa fa-map-marker mr-2"></em>{{$edu->schoolLocation}}</span>
                                    <span class="history-start-end"><em class="fa fa-calendar mr-2"></em>{{$edu->graduationStart}} - {{$edu->graduationEnd}}</span>
                                </div>
                                <p>Sajjad has been truly great for us. Fast delivery, quick to post changes when needed and overall a great designer. Would definitely hire again!</p>
                            </li>
                        @endforeach

                    </ul>
                    </div>
                    <div class="col-12 mb-3">
                      <h4 class="font-weight-bold text-capitalize">Certificate</h4>
                      <ul class="list-unstyled profileSectList">
                        @foreach($certificate as $cert)
                            <li>
                                <h6 class="font-weight-bold">{{$cert->certificateName}}</h6>
                                <div class="d-flex justify-content-between mb-3">
                                    <span class="history-company"><em class="fa fa-building mr-2"></em>{{$cert->certificate_area}}</span>
                                    <span class="history-start-end"><em class="fa fa-calendar mr-2"></em>{{$cert->certificDate}}</span>
                                </div>
                                <p>{{$cert->certificate_summary}}</p>
                            </li>
                        @endforeach
                      </ul>
                    </div>
                    <div class="col-12 mb-3">
                        <h4 class="font-weight-bold text-capitalize">Additiona Information</h4>
                        <p>{{$usersInfo->AdditionalNformation}}</p>
                    </div>


                </div>
            </div>



            </div>
        </div>
   </div>

<a href="{{ route('download-pdf') }}" target= "_blank">Save as PDF</a>


  </body>
</html>
